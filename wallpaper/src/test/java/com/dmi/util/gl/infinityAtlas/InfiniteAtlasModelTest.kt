package com.dmi.util.gl.infinityAtlas

import com.dmi.util.*
import com.dmi.util.gl.infinityAtlas.InfiniteAtlasModel.Pieces
import com.dmi.util.gl.obj.Projection2D
import org.amshove.kluent.shouldBeEqualTo
import org.junit.jupiter.api.Test

class InfiniteAtlasModelTest {
    private val worldSize = Vec2(2F, 2F)
    private val model = IdentityAtlasModel(textureSize = Pos2(100, 1000), worldSize = worldSize)

    @Test
    fun `full region, without offset`() {
        model.piecesOf(Vec2(0F, 0F)).check(
            projections = Projections(Vec2(0F, 0F), scale = 1F),
            clips = Clips(Vec2(0F, 0F)..Vec2(1F, 1F))
        )
    }

    @Test
    fun `full region, small offset`() {
        model.piecesOf(Vec2(0.5F, 1.5F)).check(
            projections = Projections(Vec2(0.5F, 1.5F), scale = 1F),
            clips = Clips(Vec2(0.25F, 0.75F)..Vec2(1.25F, 1.75F))
        )
    }

    @Test
    fun `full region, large offset`() {
        model.piecesOf(Vec2(1.5F, 4.5F)).check(
            projections = Projections(Vec2(1.5F, 0.5F), scale = 1F),
            clips = Clips(Vec2(0.75F, 0.25F)..Vec2(1.75F, 1.25F))
        )
    }

    @Test
    fun `extended atlas, without offset`() {
        model.extended(2F).piecesOf(Vec2(0F, 0F)).check(
            projections = Projections(Vec2(0F, 0F), scale = 0.5F),
            clips = Clips(Vec2(0F, 0F)..Vec2(1F, 1F))
        )
    }

    @Test
    fun `extended atlas, large offset`() {
        model.extended(2F).piecesOf(Vec2(1F, 3F)).check(
            projections = Projections(Vec2(1F / 2F, 3F / 2F), scale = 0.5F),
            clips = Clips(Vec2(0.25F, 0.75F)..Vec2(1.25F, 1.75F))
        )
        model.extended(2F).piecesOf(Vec2(1F, 4F)).check(
            projections = Projections(Vec2(1F / 2F, 0F), scale = 0.5F),
            clips = Clips(Vec2(0.25F, 0F)..Vec2(1.25F, 1F))
        )
    }

    @Test
    fun `extended atlas, large offset, clip`() {
        model.extended(2F).piecesOf(Vec2(1F, 3F), (Vec2(0F, 0F)..Vec2(1F, 1F)) * 4F).check(
            projections = Projections(Vec2(1F / 2F, 3F / 2F), scale = 0.5F),
            clips = Clips(Vec2(0.25F, 0.75F)..Vec2(1.25F, 1.75F))
        )
        model.extended(2F).piecesOf(Vec2(1F, 4F), (Vec2(0F, 0F)..Vec2(1F, 1F)) * 4F).check(
            projections = Projections(Vec2(1F / 2F, 0F), scale = 0.5F),
            clips = Clips(Vec2(0.25F, 0F)..Vec2(1.25F, 1F))
        )

        model.extended(2F).piecesOf(Vec2(1F, 3F), (Vec2(0F, 0F)..Vec2(0.5F, 0.5F)) * 4F).check(
            projections = Projections(Vec2(1F / 2F, 3F / 2F), scale = 0.5F),
            clips = Clips(Vec2(0.25F, 0.75F)..Vec2(0.75F, 1.25F))
        )
        model.extended(2F).piecesOf(Vec2(1F, 4F), (Vec2(0F, 0F)..Vec2(0.5F, 0.5F)) * 4F).check(
            projections = Projections(Vec2(1F / 2F, 0F), scale = 0.5F),
            clips = Clips(Vec2(0.25F, 0F)..Vec2(0.75F, 0.5F))
        )

        model.extended(2F).piecesOf(Vec2(1F, 3F), (Vec2(0.5F, 0.5F)..Vec2(1F, 1F)) * 4F).check(
            projections = Projections(Vec2(1F / 2F, 3F / 2F), scale = 0.5F),
            clips = Clips(Vec2(0.75F, 1.25F)..Vec2(1.25F, 1.75F))
        )
        model.extended(2F).piecesOf(Vec2(1F, 4F), (Vec2(0.5F, 0.5F)..Vec2(1F, 1F)) * 4F).check(
            projections = Projections(Vec2(1F / 2F, 0F), scale = 0.5F),
            clips = Clips(Vec2(0.75F, 0.5F)..Vec2(1.25F, 1F))
        )
    }

    @Test
    fun textureRegion() {
        model.textureRegionOf(Vec2(-2F, -2F)) shouldBeEqualTo Vec2(0F, 0F)..Vec2(1F, 1F)
        model.textureRegionOf(Vec2(-1F, -1F)) shouldBeEqualTo Vec2(0.5F, 0.5F)..Vec2(1.5F, 1.5F)
        model.textureRegionOf(Vec2(0F, 0F)) shouldBeEqualTo Vec2(0F, 0F)..Vec2(1F, 1F)
        model.textureRegionOf(Vec2(1F, 1F)) shouldBeEqualTo Vec2(0.5F, 0.5F)..Vec2(1.5F, 1.5F)
        model.textureRegionOf(Vec2(2F, 2F)) shouldBeEqualTo Vec2(0F, 0F)..Vec2(1F, 1F)
    }

    @Test
    fun `textureRegion of extended`() {
        model.extended(2F).textureRegionOf(Vec2(-4F, -4F)) shouldBeEqualTo Vec2(0.25F, 0.25F)..Vec2(0.75F, 0.75F)
        model.extended(2F).textureRegionOf(Vec2(-2F, -2F)) shouldBeEqualTo Vec2(0.75F, 0.75F)..Vec2(1.25F, 1.25F)
        model.extended(2F).textureRegionOf(Vec2(0F, 0F)) shouldBeEqualTo Vec2(0.25F, 0.25F)..Vec2(0.75F, 0.75F)
        model.extended(2F).textureRegionOf(Vec2(2F, 2F)) shouldBeEqualTo Vec2(0.75F, 0.75F)..Vec2(1.25F, 1.25F)
        model.extended(2F).textureRegionOf(Vec2(4F, 4F)) shouldBeEqualTo Vec2(0.25F, 0.25F)..Vec2(0.75F, 0.75F)
    }

    private fun Pieces.check(projections: Projections, clips: Clips) {
        leftBottom.projection shouldBeEqualTo projections.leftBottom
        rightBottom.projection shouldBeEqualTo projections.rightBottom
        leftTop.projection shouldBeEqualTo projections.leftTop
        rightTop.projection shouldBeEqualTo projections.rightTop
//        leftBottom.clip shouldBeEqualTo clips.leftBottom
//        rightBottom.clip shouldBeEqualTo clips.rightBottom
//        leftTop.clip shouldBeEqualTo clips.leftTop
        rightTop.clip shouldBeEqualTo clips.rightTop
    }

    private inner class Projections(offset: Vec2, scale: Float) {
        val leftBottom = Projection2D(offset - worldSize, scale)
        val rightBottom = Projection2D(offset - Vec2(0F, worldSize.y), scale)
        val leftTop = Projection2D(offset - Vec2(worldSize.x, 0F), scale)
        val rightTop = Projection2D(offset - Vec2(0F, 0F), scale)
    }

    private class Clips(region: Region2) {
        val leftBottom = (region - Vec2(1F, 1F)).coerceIn(Region2.ONE)
        val rightBottom = (region - Vec2(0F, 1F)).coerceIn(Region2.ONE)
        val leftTop = (region - Vec2(1F, 0F)).coerceIn(Region2.ONE)
        val rightTop = (region - Vec2(0F, 0F)).coerceIn(Region2.ONE)
    }
}
