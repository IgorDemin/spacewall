package com.dmi.util

import org.amshove.kluent.shouldBeEqualTo
import org.junit.jupiter.api.Test

class AlgebraTest {
    @Test
    fun region2Intersection() {
        (Vec2.ZERO..Vec2.ONE) intersection (Vec2.ZERO..2F * Vec2.ONE) shouldBeEqualTo (Vec2.ZERO..Vec2.ONE)
        (Vec2.ZERO..2F * Vec2.ONE) intersection (Vec2.ZERO..Vec2.ONE) shouldBeEqualTo (Vec2.ZERO..Vec2.ONE)
        (-Vec2.ONE..Vec2.ONE) intersection (Vec2.ZERO..Vec2.ONE) shouldBeEqualTo (Vec2.ZERO..Vec2.ONE)
    }

    @Test
    fun `diffAfterOffset small`() {
        diffAfterOffset(
            region = Vec2(12F, 14F)..Vec2(20F, 18F),
            offset = Vec2(0F, 0F)
        ).apply {
            first shouldBeEqualTo Vec2(12F, 18F)..Vec2(20F, 18F)
            second shouldBeEqualTo Vec2(20F, 14F)..Vec2(20F, 18F)
        }

        diffAfterOffset(
            region = Vec2(12F, 14F)..Vec2(20F, 18F),
            offset = Vec2(1F, 2F)
        ).apply {
            first shouldBeEqualTo Vec2(12F + 1F, 18F)..Vec2(20F + 1F, 18F + 2F)
            second shouldBeEqualTo Vec2(20F, 14F + 2F)..Vec2(20F + 1F, 18F)
        }

        diffAfterOffset(
            region = Vec2(12F, 14F)..Vec2(20F, 18F),
            offset = Vec2(-1F, 2F)
        ).apply {
            first shouldBeEqualTo Vec2(12F - 1F, 18F)..Vec2(20F - 1F, 18F + 2F)
            second shouldBeEqualTo Vec2(12F - 1F, 14F + 2F)..Vec2(12F, 18F)
        }

        diffAfterOffset(
            region = Vec2(12F, 14F)..Vec2(20F, 18F),
            offset = Vec2(1F, -2F)
        ).apply {
            first shouldBeEqualTo Vec2(12F + 1F, 14F - 2F)..Vec2(20F + 1F, 14F)
            second shouldBeEqualTo Vec2(20F, 14F)..Vec2(20F + 1F, 18F - 2F)
        }

        diffAfterOffset(
            region = Vec2(12F, 14F)..Vec2(20F, 18F),
            offset = Vec2(-1F, -2F)
        ).apply {
            first shouldBeEqualTo Vec2(12F - 1F, 14F - 2F)..Vec2(20F - 1F, 14F)
            second shouldBeEqualTo Vec2(12F - 1F, 14F)..Vec2(12F, 18F - 2F)
        }
    }

    @Test
    fun `diffAfterOffset big`() {
        diffAfterOffset(
            region = Vec2(12F, 14F)..Vec2(20F, 18F),
            offset = Vec2(10F, 20F)
        ).apply {
            first shouldBeEqualTo Vec2(12F + 10F, 14F + 20F)..Vec2(20F + 10F, 18F + 20F)
            second shouldBeEqualTo Vec2(12F + 10F, 14F + 20F)..Vec2(20F + 10F, 14F + 20F)
        }

        diffAfterOffset(
            region = Vec2(12F, 14F)..Vec2(20F, 18F),
            offset = Vec2(-10F, -20F)
        ).apply {
            first shouldBeEqualTo Vec2(12F - 10F, 14F - 20F)..Vec2(20F - 10F, 18F - 20F)
            second shouldBeEqualTo Vec2(12F - 10F, 18F - 20F)..Vec2(20F - 10F, 18F - 20F)
        }

        diffAfterOffset(
            region = Vec2(12F, 14F)..Vec2(20F, 18F),
            offset = Vec2(-10F, 20F)
        ).apply {
            first shouldBeEqualTo Vec2(12F - 10F, 14F + 20F)..Vec2(20F - 10F, 18F + 20F)
            second shouldBeEqualTo Vec2(12F - 10F, 14F + 20F)..Vec2(20F - 10F, 14F + 20F)
        }

        diffAfterOffset(
            region = Vec2(12F, 14F)..Vec2(20F, 18F),
            offset = Vec2(10F, -20F)
        ).apply {
            first shouldBeEqualTo Vec2(12F + 10F, 14F - 20F)..Vec2(20F + 10F, 18F - 20F)
            second shouldBeEqualTo Vec2(12F + 10F, 18F - 20F)..Vec2(20F + 10F, 18F - 20F)
        }
    }
}