package com.dmi.util

import org.amshove.kluent.shouldBeEqualTo
import org.junit.jupiter.api.Test

class RangesTest {
    @Test
    fun openClosed() {
        0.9F shouldNotBeIn open(1F)..closed(2F)
        1F shouldNotBeIn open(1F)..closed(2F)
        1.1F shouldBeIn open(1F)..closed(2F)
        1.9F shouldBeIn open(1F)..closed(2F)
        2F shouldBeIn open(1F)..closed(2F)
        2.1F shouldNotBeIn open(1F)..closed(2F)
    }

    @Test
    fun closedOpen() {
        0.9F shouldNotBeIn closed(1F)..open(2F)
        1F shouldBeIn closed(1F)..open(2F)
        1.1F shouldBeIn closed(1F)..open(2F)
        1.9F shouldBeIn closed(1F)..open(2F)
        2F shouldNotBeIn closed(1F)..open(2F)
        2.1F shouldNotBeIn closed(1F)..open(2F)
    }

    private infix fun Float.shouldBeIn(range: RangeF) = (this in range) shouldBeEqualTo true
    private infix fun Float.shouldNotBeIn(range: RangeF) = (this in range) shouldBeEqualTo false
}