precision mediump float;

varying vec2 fragmentPosition;
uniform sampler2D texture;

void main() {
    gl_FragColor = texture2D(texture, fragmentPosition);
}