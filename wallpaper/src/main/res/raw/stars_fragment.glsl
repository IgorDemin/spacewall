// оригинал https://www.shadertoy.com/view/XlfGRj (Star Nest)

precision highp float;

uniform int iterations;
uniform float param1;
uniform float param2;
uniform float param3;
uniform float param4;
uniform float tile;
uniform float intensity;
uniform float gamma;

varying vec3 point;

void main(){
    vec3 p = abs(vec3(tile) - mod(point, vec3(tile * 2.)));

    float old, current, sum = 0.;
    for (int i = 0; i < iterations; i++) {
        p = param1 * pow(abs(p), vec3(param4, param4, param4)) / dot(p, p) - param2;
        current = length(p);
        sum += pow(abs(current - old), param3);
        old = current;
    }

    // normalize
    sum = sum * 17. / float(iterations);

    vec3 a = vec3(pow(sum, gamma) * intensity);
    gl_FragColor = vec4(a, 1.);
}
