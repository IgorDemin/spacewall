precision mediump float;

attribute vec4 position;

uniform vec2 textureOffset;
uniform vec2 textureSize;

varying vec2 fragmentPosition;

void main() {
    gl_Position = vec4(position.xy, -1, 1);
    fragmentPosition = textureOffset + position.zw * textureSize;
}