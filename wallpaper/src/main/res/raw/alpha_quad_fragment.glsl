precision mediump float;

varying vec2 fragmentPosition;
uniform sampler2D texture;
uniform vec4 color;

void main() {
    float intensity = texture2D(texture, fragmentPosition).r;
    gl_FragColor = color * intensity;
}