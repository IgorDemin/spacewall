precision highp float;

uniform vec2 projectionOffset;
uniform float projectionScale;
uniform vec3 windowOffset;
uniform vec2 windowSize;
uniform vec2 angle;

attribute vec2 vertexPosition;

varying vec3 point;

void main() {
    vec3 direction = vec3(vertexPosition.x * windowSize.x / 2., vertexPosition.y * windowSize.y / 2., 0.) / projectionScale;
    point = windowOffset + direction;

    mat2 rot1 = mat2(cos(angle.x), sin(angle.x), -sin(angle.x), cos(angle.x));
    mat2 rot2 = mat2(cos(angle.y), sin(angle.y), -sin(angle.y), cos(angle.y));
    point.xz *= rot1;
    point.xy *= rot2;

    gl_Position = vec4(projectionOffset + vertexPosition, -1., 1.);
}
