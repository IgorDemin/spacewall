package com.dmi.spacewall.ui.settings.preset

import androidx.compose.Composable
import androidx.ui.core.Alignment
import androidx.ui.core.Modifier
import androidx.ui.foundation.Box
import androidx.ui.foundation.Text
import androidx.ui.layout.fillMaxWidth
import androidx.ui.material.Button
import androidx.ui.res.stringResource
import androidx.ui.unit.dp
import com.dmi.spacewall.R
import com.dmi.spacewall.ui.common.Ambients
import com.dmi.spacewall.ui.common.PlaceNavigation
import com.dmi.spacewall.ui.settings.common.WallpaperDialogScreen

@Composable
fun PresetPreview(
    navigation: PlaceNavigation = Ambients.Navigation.current
) = WallpaperDialogScreen {
    Box(padding = 16.dp, modifier = Modifier.fillMaxWidth(), gravity = Alignment.Center) {
        Button(onClick = navigation::exit) {
            Text(stringResource(R.string.close))
        }
    }
}
