package com.dmi.spacewall.ui.settings

import androidx.compose.Composable
import androidx.ui.core.Alignment
import androidx.ui.core.Modifier
import androidx.ui.foundation.Box
import androidx.ui.foundation.Text
import androidx.ui.layout.*
import androidx.ui.material.AlertDialog
import androidx.ui.material.MaterialTheme
import androidx.ui.material.TextButton
import androidx.ui.res.stringResource
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import com.dmi.spacewall.R
import com.dmi.spacewall.domain.World
import com.dmi.spacewall.ui.common.*

@Composable
fun ResetSettingsScreen(
    navigation: PlaceNavigation = Ambients.Navigation.current,
    world: World = currentDomain.world
) = AlertDialog(
    onCloseRequest = navigation::exit,
    text = {
        Column {
            Text(stringResource(R.string.settings_reset_title), style = MaterialTheme.typography.h6, color = AppColor.black87)

            Spacer(modifier = Modifier.height(16.dp))

            Text(stringResource(R.string.settings_reset_confirmation))
        }
    },
    buttons = {
        Box(modifier = Modifier.fillMaxWidth(), gravity = Alignment.BottomEnd) {
            Row {
                TextButton(
                    onClick = navigation::exit,
                    modifier = Modifier.padding(end = 16.dp, bottom = 16.dp)
                ) {
                    Text(stringResource(R.string.cancel_upper))
                }

                TextButton(
                    onClick = { world.reset(); navigation.exit() },
                    modifier = Modifier.padding(end = 16.dp, bottom = 16.dp)
                ) {
                    Text(stringResource(R.string.ok_upper))
                }
            }
        }
    }
)

@Preview
@Composable
fun ResetSettingsScreenPreview() = PreviewScreen { ResetSettingsScreen() }
