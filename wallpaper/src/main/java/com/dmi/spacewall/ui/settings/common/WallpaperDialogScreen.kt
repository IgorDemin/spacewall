package com.dmi.spacewall.ui.settings.common

import androidx.compose.Composable
import androidx.ui.core.Modifier
import androidx.ui.foundation.Box
import androidx.ui.foundation.Clickable
import androidx.ui.foundation.VerticalScroller
import androidx.ui.layout.Column
import androidx.ui.layout.fillMaxSize
import androidx.ui.layout.fillMaxWidth
import androidx.ui.layout.padding
import androidx.ui.material.Surface
import androidx.ui.unit.dp
import com.dmi.spacewall.ui.common.Ambients
import com.dmi.spacewall.ui.common.PlaceNavigation

@Composable
fun WallpaperDialogScreen(
    navigation: PlaceNavigation = Ambients.Navigation.current,
    content: @Composable() () -> Unit
) {
    Column(modifier = Modifier.fillMaxSize()) {
        Clickable(
            navigation::exit,
            modifier = Modifier.weight(1F).fillMaxSize()
        ) {
            Box()
        }
        Surface {
            VerticalScroller(modifier = Modifier.fillMaxWidth().padding(top = 16.dp, bottom = 12.dp)) {
                Column {
                    content()
                }
            }
        }
    }
}
