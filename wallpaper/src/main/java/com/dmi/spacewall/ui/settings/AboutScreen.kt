package com.dmi.spacewall.ui.settings

import androidx.compose.Composable
import androidx.ui.core.Alignment
import androidx.ui.core.Modifier
import androidx.ui.foundation.Clickable
import androidx.ui.foundation.Text
import androidx.ui.layout.*
import androidx.ui.material.AlertDialog
import androidx.ui.material.MaterialTheme
import androidx.ui.material.TextButton
import androidx.ui.res.stringResource
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import com.dmi.spacewall.R
import com.dmi.spacewall.ui.common.Ambients
import com.dmi.spacewall.ui.common.AppColor
import com.dmi.spacewall.ui.common.PlaceNavigation
import com.dmi.spacewall.ui.common.PreviewScreen
import com.dmi.util.ui.*


@Composable
fun AboutScreen(
    navigation: PlaceNavigation = Ambients.Navigation.current
) {
    val activity = ActivityAmbient.current
    val link1 = textLinkFrom(R.string.about_text2)
    val link2 = textLinkFrom(R.string.about_text3)

    AlertDialog(
        onCloseRequest = navigation::exit,
        text = {
            Column {
                Text(stringResource(R.string.about_title), style = MaterialTheme.typography.h6, color = AppColor.black87)

                Spacer(modifier = Modifier.height(16.dp))

                Text(stringResource(R.string.about_text0, activity.appVersion()))

                Spacer(modifier = Modifier.height(16.dp))

                Text(stringResource(R.string.about_text1))

                Spacer(modifier = Modifier.height(16.dp))

                Clickable(onClick = { activity.openLink(link1) }) {
                    Text(textResource(R.string.about_text2))
                }

                Spacer(modifier = Modifier.height(16.dp))

                Clickable(onClick = { activity.openLink(link2) }) {
                    Text(textResource(R.string.about_text3))
                }
            }
        },
        buttons = {
            Column(modifier = Modifier.fillMaxWidth()) {
                TextButton(
                    onClick = navigation::exit,
                    modifier = Modifier.padding(end = 16.dp, bottom = 16.dp).gravity(Alignment.End)
                ) {
                    Text(stringResource(R.string.ok_upper))
                }
            }
        }
    )
}

@Preview
@Composable
fun AboutScreenPreview() = PreviewScreen { AboutScreen() }
