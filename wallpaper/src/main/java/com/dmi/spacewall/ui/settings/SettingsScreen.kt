package com.dmi.spacewall.ui.settings

import androidx.compose.Composable
import androidx.compose.Providers
import androidx.ui.core.Modifier
import androidx.ui.foundation.ContentColorAmbient
import androidx.ui.foundation.Icon
import androidx.ui.foundation.Text
import androidx.ui.layout.Column
import androidx.ui.layout.padding
import androidx.ui.material.*
import androidx.ui.material.icons.Icons
import androidx.ui.material.icons.outlined.ArrowBack
import androidx.ui.material.icons.outlined.Close
import androidx.ui.res.stringResource
import androidx.ui.unit.dp
import com.dmi.spacewall.R
import com.dmi.spacewall.presentation.PresetsPresentation
import com.dmi.spacewall.presentation.SettingTab
import com.dmi.spacewall.presentation.SettingsPresentation
import com.dmi.spacewall.ui.common.*
import com.dmi.spacewall.ui.settings.categories.CategoriesScreen
import com.dmi.spacewall.ui.settings.preset.PresetsScreen
import com.dmi.util.async.Async
import com.dmi.util.ui.LoadingView
import com.dmi.util.ui.withTextColor

@Composable
fun SettingsScreen(presentation: SettingsPresentation) {
    Providers(PresentationAmbients.Settings provides presentation) {
        Scaffold(
            topAppBar = { SettingsBar() },
            bodyContent = {
                Column {
                    SettingsTabs(presentation)
                    when (presentation.tab) {
                        SettingTab.Presets -> PresetsScreen()
                        SettingTab.Custom -> CategoriesScreen()
                    }
                }
            }
        )
    }
}

@Composable
private fun SettingsBar(
    presetsPresentation: Async<PresetsPresentation> = PresentationAmbients.Settings.current.presets
) = LoadingView(
    presetsPresentation,
    onLoading = { NormalSettingsBar() },
    onLoaded = { SettingsBar(it) }
)

@Composable
private fun SettingsBar(
    presetsPresentation: PresetsPresentation
) = when {
    presetsPresentation.isSelectMode -> MultiselectSettingsBar(presetsPresentation)
    else -> NormalSettingsBar()
}

@Composable
private fun NormalSettingsBar(
    navigation: PlaceNavigation = Ambients.Navigation.current
) = TopAppBar(
    elevation = 0.dp,
    title = {
        Text(stringResource(R.string.settings_title))
    },
    navigationIcon = {
        IconButton(onClick = navigation::exit) {
            Icon(Icons.Outlined.ArrowBack)
        }
    },
    actions = {
        withTextColor(MaterialTheme.colors.onPrimary) {
            TextButton(
                onClick = { navigation.go(Place.ResetSettings) },
                contentColor = ContentColorAmbient.current,
                modifier = Modifier.padding(start = 8.dp, end = 8.dp)
            ) {
                Text(stringResource(R.string.settings_reset))
            }
        }
    }
)

@Composable
private fun MultiselectSettingsBar(
    presetsPresentation: PresetsPresentation
) = TopAppBar(
    backgroundColor = AppColor.blackSolid10,
    contentColor = MaterialTheme.colors.onPrimary,
    elevation = 0.dp,
    title = {
        Text(stringResource(R.string.settings_multiselect_title, presetsPresentation.selectedCount))
    },
    navigationIcon = {
        IconButton(onClick = presetsPresentation::cancelSelectionMode) {
            Icon(Icons.Outlined.Close)
        }
    },
    actions = {
        withTextColor(MaterialTheme.colors.onPrimary) {
            TextButton(
                onClick = presetsPresentation::removeSelected,
                contentColor = ContentColorAmbient.current,
                modifier = Modifier.padding(start = 8.dp, end = 8.dp)
            ) {
                Text(stringResource(R.string.settings_multiselect_remove))
            }
        }
    }
)

@Composable
private fun SettingsTabs(settings: SettingsPresentation) = TabRow(
    items = SettingTab.values().map { stringResource(it.nameRes) },
    selectedIndex = settings.tab.ordinal
) { index, title ->
    Tab(
        text = { Text(title) },
        selected = index == settings.tab.ordinal,
        onSelected = {
            settings.tab = SettingTab.values()[index]
        }
    )
}