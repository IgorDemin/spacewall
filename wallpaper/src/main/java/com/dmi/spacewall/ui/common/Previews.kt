package com.dmi.spacewall.ui.common

import androidx.compose.Composable
import androidx.compose.Providers
import androidx.ui.graphics.Color
import androidx.ui.material.MaterialTheme
import androidx.ui.material.Surface

@Composable
fun PreviewScreen(
    background: Color = MaterialTheme.colors.surface,
    body: @Composable() () -> Unit
) = Providers {
    AppTheme {
        Surface(color = background) {
            body()
        }
    }
}
