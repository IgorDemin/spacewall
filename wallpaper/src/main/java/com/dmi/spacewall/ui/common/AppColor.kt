package com.dmi.spacewall.ui.common

import androidx.ui.graphics.Color

object AppColor {
    val black87 = Color.Black.copy(alpha = 0.87F)
    val black54 = Color.Black.copy(alpha = 0.54F)
    val black38 = Color.Black.copy(alpha = 0.38F)

    val blackSolid10 = Color(0xFF1B1B1B)
}
