package com.dmi.spacewall.ui.settings.categories

import androidx.annotation.StringRes
import androidx.compose.Composable
import androidx.compose.getValue
import androidx.compose.setValue
import androidx.compose.state
import androidx.ui.core.Alignment
import androidx.ui.core.Modifier
import androidx.ui.foundation.Icon
import androidx.ui.foundation.Text
import androidx.ui.layout.Column
import androidx.ui.layout.Row
import androidx.ui.layout.fillMaxWidth
import androidx.ui.layout.padding
import androidx.ui.material.IconToggleButton
import androidx.ui.material.Slider
import androidx.ui.material.Switch
import androidx.ui.material.ripple.ripple
import androidx.ui.res.stringResource
import androidx.ui.res.vectorResource
import androidx.ui.tooling.preview.Preview
import androidx.ui.unit.dp
import arrow.optics.Lens
import com.dmi.spacewall.R
import com.dmi.spacewall.domain.Config
import com.dmi.spacewall.domain.World
import com.dmi.spacewall.ui.common.PreviewScreen
import com.dmi.spacewall.ui.common.currentDomain
import com.dmi.util.FloatPair
import com.dmi.util.immutable
import com.dmi.util.ui.widget.PairSlider
import com.dmi.util.ui.widget.RangeValue

@Composable
fun ToggleSetting(
    @StringRes nameRes: Int = R.string.settings_detail_performance_mainIsAntialiasing,
    lens: Lens<Config, Boolean> = Lens.immutable(false),
    world: World = currentDomain.world
) = Row(modifier = Modifier.padding(start = 12.dp, end = 12.dp, top = 16.dp, bottom = 16.dp).fillMaxWidth().ripple()) {
    Text(
        text = stringResource(nameRes),
        modifier = Modifier.padding(start = 8.dp, end = 8.dp).weight(1F)
    )

    Switch(
        checked = lens.get(world.config),
        onCheckedChange = {
            world.config = lens.set(world.config, it)
        }
    )
}

@Composable
fun SliderSetting(
    @StringRes nameRes: Int = R.string.settings_detail_performance_mainResolution,
    lens: Lens<Config, Float> = Lens.immutable(0.5F),
    range: ClosedFloatingPointRange<Float> = 0F..1F,
    world: World = currentDomain.world
) = Column(modifier = Modifier.padding(start = 12.dp, end = 12.dp)) {
    Text(text = stringResource(nameRes), modifier = Modifier.padding(start = 8.dp, end = 8.dp))

    Slider(
        value = lens.get(world.config),
        onValueChange = {
            world.config = lens.set(world.config, it)
        },
        valueRange = range
    )
}

@Composable
fun SliderPairSetting(
    @StringRes nameRes: Int = R.string.settings_detail_color_backgroundHSV_x,
    lens: Lens<Config, FloatPair> = Lens.immutable(
        FloatPair(0.3F, 0.7F)
    ),
    range: ClosedFloatingPointRange<Float> = 0F..1F,
    world: World = currentDomain.world
) = Column(modifier = Modifier.padding(start = 12.dp, end = 12.dp)) {
    val pair = lens.get(world.config)

    var isPairMode by state {
        pair.start != pair.end
    }

    fun setPair(pair: FloatPair) {
        world.config = lens.set(world.config, pair)
    }

    fun setPairMode(value: Boolean) {
        val current = lens.get(world.config)
        val center = (current.end + current.start) / 2F
        if (value) {
            val tick = (range.endInclusive - range.start) / 10F
            val start = (center - tick).coerceIn(range)
            val end = (center + tick).coerceIn(range)
            setPair(FloatPair(start, end))
        } else {
            setPair(FloatPair(center, center))
        }
        isPairMode = value
    }

    Text(text = stringResource(nameRes), modifier = Modifier.padding(start = 8.dp, end = 8.dp))

    Row {
        if (isPairMode) {
            PairSlider(
                value = pair.toRangeValue(),
                onValueChange = { setPair(it.toFloatPair()) },
                modifier = Modifier.gravity(Alignment.CenterVertically).weight(1F),
                valueRange = range
            )
        } else {
            Slider(
                value = pair.start,
                onValueChange = { setPair(FloatPair(it, it)) },
                modifier = Modifier.gravity(Alignment.CenterVertically).weight(1F),
                valueRange = range
            )
        }

        IconToggleButton(
            checked = isPairMode,
            onCheckedChange = { setPairMode(it) },
            modifier = Modifier.gravity(Alignment.CenterVertically)
        ) {
            if (isPairMode) {
                Icon(vectorResource(R.drawable.ic_baseline_swap_horiz_24))
            } else {
                Icon(vectorResource(R.drawable.ic_baseline_compare_arrows_24))
            }
        }
    }
}

private fun FloatPair.toRangeValue() = RangeValue(start, end)

private fun RangeValue.toFloatPair() = FloatPair(start, end)


@Preview
@Composable
fun CheckSettingPreview() = PreviewScreen { ToggleSetting() }

@Preview
@Composable
fun SliderSettingPreview() = PreviewScreen { SliderSetting() }

@Preview
@Composable
fun SliderPairSettingPreview() = PreviewScreen { SliderPairSetting() }
