package com.dmi.spacewall.ui.common

import androidx.compose.Composable
import androidx.compose.Model
import com.dmi.spacewall.presentation.SettingsPresentation
import com.dmi.spacewall.ui.settings.categories.SettingsDetailPlace
import com.dmi.util.ui.Navigation

sealed class Place(val isFullscreen: Boolean = true) {
    @Model
    class Settings : Place() {
        private var presentation: SettingsPresentation? = null

        @Composable
        fun presentation(): SettingsPresentation {
            if (presentation == null) {
                presentation = PresentationAmbients.App.current.settings()
            }
            return presentation!!
        }
    }

    object About : Place(isFullscreen = false)
    object ResetSettings : Place(isFullscreen = false)
    class SettingsDetail(val instance: SettingsDetailPlace) : Place()
    object PresetPreview : Place()
}

typealias PlaceNavigation = Navigation<Place>
