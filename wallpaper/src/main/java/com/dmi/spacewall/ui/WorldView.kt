package com.dmi.spacewall.ui

import android.graphics.Bitmap
import com.dmi.util.Range2

interface WorldView {
    suspend fun takeScreenshot(range: Range2): Bitmap

    fun piece(range: Range2) = object : Piece {
        override suspend fun takeScreenshot(): Bitmap = takeScreenshot(range)
    }

    interface Piece {
        suspend fun takeScreenshot(): Bitmap
    }
}
