package com.dmi.spacewall.ui.settings.preset

import androidx.compose.Composable
import androidx.ui.core.*
import androidx.ui.core.gesture.longPressGestureFilter
import androidx.ui.foundation.*
import androidx.ui.graphics.Color
import androidx.ui.graphics.asImageAsset
import androidx.ui.graphics.painter.ImagePainter
import androidx.ui.layout.aspectRatio
import androidx.ui.layout.fillMaxSize
import androidx.ui.layout.padding
import androidx.ui.layout.preferredSize
import androidx.ui.material.MaterialTheme
import androidx.ui.material.OutlinedButton
import androidx.ui.material.Surface
import androidx.ui.material.ripple.ripple
import androidx.ui.res.stringResource
import androidx.ui.unit.dp
import com.dmi.spacewall.R
import com.dmi.spacewall.presentation.PresetCell
import com.dmi.spacewall.presentation.PresetPresentation
import com.dmi.spacewall.presentation.PresetsPresentation
import com.dmi.spacewall.ui.WorldView
import com.dmi.spacewall.ui.common.Ambients
import com.dmi.spacewall.ui.common.PresentationAmbients
import com.dmi.util.Pos2
import com.dmi.util.async.Async
import com.dmi.util.rangeTo
import com.dmi.util.ui.LoadingView
import com.dmi.util.ui.widget.AdapterGrid

@Composable
fun PresetsScreen(
    presetsPresentation: Async<PresetsPresentation> = PresentationAmbients.Settings.current.presets
) = Box(modifier = Modifier.fillMaxSize(), gravity = Alignment.Center) {
    LoadingView(presetsPresentation) {
        PresetsView(it)
    }
}

@Composable
private fun PresetsView(presets: PresetsPresentation) = AdapterGrid(
    presets.all,
    columns = 3,
    modifier = Modifier.padding(8.dp).fillMaxSize()
) {
    PresetCellView(it)
}

@Composable
private fun PresetCellView(cell: PresetCell) = Box(
    modifier = Modifier
        .preferredSize(128.dp)
        .padding(2.dp)
        .aspectRatio(1F),
    gravity = ContentGravity.Center
) {
    when (cell) {
        is PresetCell.Add -> PresetCellAddView(cell)
        is PresetCell.Preset -> PresetCellPresetView(cell)
    }
}

@Composable
private fun PresetCellAddView(
    cell: PresetCell.Add,
    worldView: WorldView = Ambients.WorldView.current
) {
    val size = with(DensityAmbient.current) {
        160.dp.toPx().value.toInt()
    }

    OutlinedButton(
        onClick = {
            val piece = worldView.piece(Pos2.ZERO..Pos2(size, size))
            cell.add(piece)
        }
    ) {
        Text(
            stringResource(R.string.settings_presets_add),
            color = MaterialTheme.colors.primary,
            style = MaterialTheme.typography.subtitle2
        )
    }
}

@Composable
private fun PresetCellPresetView(cell: PresetCell.Preset) {
    Box(modifier = Modifier.fillMaxSize(), gravity = Alignment.Center) {
        LoadingView(cell.preset, onLoading = {}) {
            PresetView(it)
        }
    }
}

@Composable
private fun PresetView(preset: PresetPresentation) {
    PresetButton(
        onClick = preset::onClick,
        onLongClick = preset::onLongClick,
        isSelected = preset.isSelected,
        modifier = Modifier.fillMaxSize()
    ) {
        val image = preset.image
        if (image != null) {
            Image(
                ImagePainter(image.asImageAsset()),
                contentScale = ContentScale.FillWidth,
                modifier = Modifier.fillMaxSize().clip(MaterialTheme.shapes.medium)
            )
        }
    }
}

@Composable
private fun PresetButton(
    onClick: () -> Unit = {},
    onLongClick: () -> Unit = {},
    isSelected: Boolean,
    modifier: Modifier,
    children: @Composable() () -> Unit
) = Surface(
    shape = MaterialTheme.shapes.small,
    color = if (isSelected) MaterialTheme.colors.primary.copy(alpha = 0.24F) else Color.Transparent,
    contentColor = MaterialTheme.colors.primary,
    border = if (isSelected) Border(2.dp, MaterialTheme.colors.primary) else null
) {
    Clickable(
        modifier = modifier
            .ripple()
            .longPressGestureFilter { onLongClick() },
        onClick = onClick
    ) {
        Box(modifier = modifier.padding(6.dp)) {
            children()
        }
    }
}
