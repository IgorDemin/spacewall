package com.dmi.spacewall.ui.settings.categories

import androidx.annotation.StringRes
import androidx.compose.Composable
import androidx.ui.core.Modifier
import androidx.ui.foundation.Clickable
import androidx.ui.foundation.Icon
import androidx.ui.foundation.Text
import androidx.ui.graphics.vector.VectorAsset
import androidx.ui.layout.Row
import androidx.ui.layout.fillMaxWidth
import androidx.ui.layout.padding
import androidx.ui.material.MaterialTheme
import androidx.ui.material.ripple.ripple
import androidx.ui.res.stringResource
import androidx.ui.res.vectorResource
import androidx.ui.unit.dp
import com.dmi.spacewall.ui.common.Ambients
import com.dmi.spacewall.ui.common.AppColor
import com.dmi.spacewall.ui.common.Place
import com.dmi.spacewall.ui.common.PlaceNavigation

@Composable
fun DetailCategory(place: SettingsDetailPlace) = PlaceCategory(
    vectorResource(place.iconRes),
    place.nameRes,
    Place.SettingsDetail(place)
)

@Composable
fun PlaceCategory(
    icon: VectorAsset,
    @StringRes nameRes: Int,
    place: Place,
    navigation: PlaceNavigation = Ambients.Navigation.current
) = ActionCategory(
    icon,
    nameRes,
    onClick = { navigation.go(place) }
)

@Composable
fun ActionCategory(
    icon: VectorAsset,
    @StringRes nameRes: Int,
    onClick: () -> Unit
) = Clickable(
    onClick = onClick,
    modifier = Modifier.ripple() + Modifier.fillMaxWidth()
) {
    Row(modifier = Modifier.padding(16.dp)) {
        Icon(asset = icon, tint = AppColor.black54, modifier = Modifier.padding(end = 16.dp))
        Text(stringResource(nameRes))
    }
}

@Composable
fun CategoryTitle(@StringRes nameRes: Int) = Text(
    stringResource(nameRes),
    style = MaterialTheme.typography.subtitle2.copy(color = MaterialTheme.colors.primary),
    modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 16.dp)
)
