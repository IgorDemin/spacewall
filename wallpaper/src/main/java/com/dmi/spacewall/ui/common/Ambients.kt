package com.dmi.spacewall.ui.common

import androidx.compose.Composable
import androidx.compose.ambientOf
import com.dmi.spacewall.App
import com.dmi.spacewall.AppPresentation
import com.dmi.spacewall.presentation.SettingsPresentation
import com.dmi.spacewall.ui.WorldView

object Ambients {
    val Navigation = ambientOf<PlaceNavigation>()
    val App = ambientOf<App>()
    val WorldView = ambientOf<WorldView>()
}

object PresentationAmbients {
    val App = ambientOf<AppPresentation>()
    val Settings = ambientOf<SettingsPresentation>()
}

@Composable
val currentApp
    get() = Ambients.App.current

@Composable
val currentDomain
    get() = currentApp.domain
