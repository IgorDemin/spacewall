package com.dmi.spacewall.ui

import androidx.compose.Composable
import com.dmi.spacewall.ui.common.Ambients
import com.dmi.spacewall.ui.common.AppTheme
import com.dmi.spacewall.ui.common.Place
import com.dmi.spacewall.ui.settings.AboutScreen
import com.dmi.spacewall.ui.settings.ResetSettingsScreen
import com.dmi.spacewall.ui.settings.SettingsScreen
import com.dmi.spacewall.ui.settings.preset.PresetPreview
import com.dmi.util.ui.AnimateChange
import com.dmi.util.ui.Navigation
import com.dmi.util.ui.Transitions

@Composable
fun SettingsRoot() = AppTheme {
    val visible = Ambients.Navigation.current.groupByVisible().last()

    AnimateChange(
        current = visible.first(),
        transition = Transitions.Fade
    ) {
        Screen(it)
    }

    for (place in visible.drop(1).dropLast(1)) {
        Screen(place)
    }

    if (visible.size >= 2) {
        AnimateChange(
            current = visible.last(),
            transition = Transitions.Fade
        ) {
            Screen(it)
        }
    }
}

@Composable
fun Screen(place: Place) = when (place) {
    is Place.Settings -> SettingsScreen(place.presentation())
    Place.About -> AboutScreen()
    Place.ResetSettings -> ResetSettingsScreen()
    Place.PresetPreview -> PresetPreview()
    is Place.SettingsDetail -> place.instance.Screen()
}

private fun Navigation<Place>.groupByVisible(): List<CurrentPlaces> {
    val list = ArrayList<ArrayList<Place>>()
    for (place in this.list) {
        if (list.isEmpty() || place.isFullscreen) {
            list.add(arrayListOf(place))
        } else {
            list.last().add(place)
        }
    }
    return list.map(::CurrentPlaces)
}

private class CurrentPlaces(val list: List<Place>) : List<Place> by list