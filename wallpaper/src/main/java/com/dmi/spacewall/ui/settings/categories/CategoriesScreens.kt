package com.dmi.spacewall.ui.settings.categories

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.Composable
import androidx.ui.foundation.VerticalScroller
import androidx.ui.graphics.Color
import androidx.ui.layout.Column
import androidx.ui.material.icons.Icons
import androidx.ui.material.icons.outlined.Info
import androidx.ui.res.vectorResource
import androidx.ui.tooling.preview.Preview
import com.dmi.spacewall.R
import com.dmi.spacewall.domain.*
import com.dmi.spacewall.ui.common.Place
import com.dmi.spacewall.ui.common.PreviewScreen
import com.dmi.spacewall.ui.settings.common.WallpaperDialogScreen
import com.dmi.util.ui.ActivityAmbient
import com.dmi.util.ui.openPlayMarket
import com.dmi.util.x
import com.dmi.util.y
import com.dmi.util.z

@Composable
fun CategoriesScreen() = VerticalScroller {
    val activity = ActivityAmbient.current
    Column {
        CategoryTitle(R.string.settings_common)
        DetailCategory(SettingsDetailPlace.PositionSpeed)
        DetailCategory(SettingsDetailPlace.PerformanceSteps)
        DetailCategory(SettingsDetailPlace.PerformanceResolution)

        CategoryTitle(R.string.settings_world)
        DetailCategory(SettingsDetailPlace.WorldMainParams)
        DetailCategory(SettingsDetailPlace.WorldSecondaryParams)

        CategoryTitle(R.string.settings_color)
        DetailCategory(SettingsDetailPlace.ColorBackground)
        DetailCategory(SettingsDetailPlace.ColorStarsMain)

        CategoryTitle(R.string.settings_info)
        PlaceCategory(Icons.Outlined.Info, R.string.settings_about, Place.About)

        ActionCategory(
            vectorResource(R.drawable.ic_baseline_play_arrow_24),
            R.string.settings_rate,
            activity::openPlayMarket
        )
    }
}

sealed class SettingsDetailPlace(
    @DrawableRes val iconRes: Int,
    @StringRes val nameRes: Int,
    private val content: @Composable() () -> Unit
) {
    @Composable
    fun Screen() = WallpaperDialogScreen { content() }

    object PositionSpeed : SettingsDetailPlace(R.drawable.ic_baseline_speed_24, R.string.settings_position_speed, {
        SliderSetting(
            R.string.settings_detail_position_velocity_speed,
            Config.position.velocitySpeed,
            0F..0.5F
        )
        SliderSetting(
            R.string.settings_detail_position_velocity_direction,
            Config.position.velocityAngleDegree,
            0F..360F
        )
    })

    object PerformanceSteps : SettingsDetailPlace(R.drawable.ic_baseline_clear_all_24, R.string.settings_performance_steps, {
        SliderSetting(
            R.string.settings_detail_position_zoom,
            Config.position.zoom,
            0.2F..4F
        )
        SliderSetting(
            R.string.settings_detail_performance_stepsFloat,
            Config.performance.stepsFloat,
            1F..30F
        )
    })

    object PerformanceResolution :
        SettingsDetailPlace(R.drawable.ic_baseline_aspect_ratio_24, R.string.settings_performance_screenResolution, {
            ToggleSetting(
                R.string.settings_detail_performance_mainIsAntialiasing,
                Config.performance.mainIsAntialiasing
            )
            SliderSetting(
                R.string.settings_detail_performance_mainResolution,
                Config.performance.mainResolution,
                0.02F..1F
            )
            SliderPairSetting(
                R.string.settings_detail_performance_starsResolution,
                Config.performance.starsResolution,
                0.02F..1F
            )
        })

    object WorldMainParams : SettingsDetailPlace(R.drawable.ic_baseline_language_24, R.string.settings_world_mainParams, {
        SliderPairSetting(
            R.string.settings_detail_world_iterations,
            Config.world.iterations,
            1F..100F
        )
        SliderPairSetting(
            R.string.settings_detail_color_gamma,
            Config.color.gamma,
            0.2F..5F
        )
    })

    object WorldSecondaryParams : SettingsDetailPlace(R.drawable.ic_baseline_waves_24, R.string.settings_world_secondaryParams, {
        SliderPairSetting(
            R.string.settings_detail_color_intensityPow,
            Config.color.intensityPow,
            -4F..4F
        )
        SliderPairSetting(
            R.string.settings_detail_world_param1,
            Config.world.param1,
            -4F..4F
        )
        SliderPairSetting(
            R.string.settings_detail_world_param2,
            Config.world.param2,
            -1F..2F
        )
        SliderPairSetting(
            R.string.settings_detail_world_param3,
            Config.world.param3,
            -4F..4F
        )
        SliderPairSetting(
            R.string.settings_detail_world_param4,
            Config.world.param4,
            -2F..2F
        )
    })

    object ColorBackground : SettingsDetailPlace(R.drawable.ic_baseline_wallpaper_24, R.string.settings_color_background, {
        SliderSetting(
            R.string.settings_detail_color_backgroundHSV_x,
            Config.color.backgroundHSV.x,
            0F..1F
        )
        SliderSetting(
            R.string.settings_detail_color_backgroundHSV_y,
            Config.color.backgroundHSV.y,
            0F..1F
        )
        SliderSetting(
            R.string.settings_detail_color_backgroundHSV_z,
            Config.color.backgroundHSV.z,
            0F..1F
        )
    })

    object ColorStarsMain : SettingsDetailPlace(R.drawable.ic_baseline_color_lens_24, R.string.settings_color_starsMain, {
        SliderPairSetting(
            R.string.settings_detail_color_hue,
            Config.color.hue,
            0F..1F
        )
        SliderPairSetting(
            R.string.settings_detail_color_saturation,
            Config.color.saturation,
            -1F..1F
        )
        SliderPairSetting(
            R.string.settings_detail_color_value,
            Config.color.value,
            0F..1F
        )
        SliderPairSetting(
            R.string.settings_detail_color_alpha,
            Config.color.alpha,
            0F..1F
        )
    })
}

@Preview
@Composable
fun CategoriesScreenPreview() = PreviewScreen { CategoriesScreen() }

@Preview
@Composable
fun PositionZoomScreenPreview() = PreviewScreen(background = Color.LightGray) { SettingsDetailPlace.PerformanceResolution.Screen() }

