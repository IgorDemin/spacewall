package com.dmi.spacewall.presentation

import androidx.annotation.StringRes
import androidx.compose.Model
import com.dmi.spacewall.AppPresentation
import com.dmi.spacewall.R
import com.dmi.util.async.cache
import com.dmi.util.injectAsync

@Model
class SettingsPresentation(val app: AppPresentation) {
    var tab: SettingTab = SettingTab.Presets

    val presets = injectAsync(::PresetsPresentation).cache()
}

enum class SettingTab(@StringRes val nameRes: Int) {
    Presets(R.string.settings_tab_presets),
    Custom(R.string.settings_tab_custom)
}
