package com.dmi.spacewall.presentation

import android.graphics.Bitmap
import androidx.compose.Model
import com.dmi.spacewall.common.Analytics
import com.dmi.spacewall.common.SafeAsync
import com.dmi.spacewall.data.PresetRepository
import com.dmi.spacewall.domain.Config
import com.dmi.spacewall.domain.Presets
import com.dmi.spacewall.domain.World
import com.dmi.spacewall.ui.WorldView
import com.dmi.spacewall.ui.common.Place
import com.dmi.spacewall.ui.common.PlaceNavigation
import com.dmi.util.Pos2
import com.dmi.util.async.Async
import com.dmi.util.async.asAsync
import com.dmi.util.async.cache
import com.dmi.util.coroutine.RootCoroutineScope
import com.dmi.util.io.InputSource
import com.dmi.util.ui.downsizeInteger
import com.dmi.util.ui.loadBitmap
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.coroutineScope
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import com.dmi.spacewall.domain.Preset as DomainPreset

private const val presetImageMaxSizePx = 320
private const val presetImageQuality = 90

suspend fun PresetsPresentation(settings: SettingsPresentation): PresetsPresentation = PresetsPresentationImpl(
    settings.app.app.data.presetRepository.all.await(),
    settings.app.navigation,
    settings.app.app.domain.world,
    settings.app.app.data.presetRepository,
    settings.app.app.analytics
)

interface PresetsPresentation {
    val all: List<PresetCell>
    val isSelectMode: Boolean
    val selectedCount: Int
    fun cancelSelectionMode()
    fun removeSelected()
}

sealed class PresetCell {
    abstract class Add : PresetCell() {
        abstract fun add(worldViewPiece: WorldView.Piece)
    }

    abstract class Preset : PresetCell() {
        abstract val preset: Async<PresetPresentation>
    }
}

interface PresetPresentation {
    val image: Bitmap?
    val isSelected: Boolean
    fun onClick()
    fun onLongClick()
}

@Model
private class PresetsPresentationImpl(
    presets: Presets,
    private val navigation: PlaceNavigation,
    private val world: World,
    private val repository: PresetRepository,
    override val analytics: Analytics
) : PresetsPresentation, SafeAsync {
    private val scope = RootCoroutineScope()

    private var presetCells = presets.map { PresetCellPresetImpl(it.asAsync()) }

    private val cellAdd = object : PresetCell.Add() {
        override fun add(worldViewPiece: WorldView.Piece) {
            val preset = addPreset(worldViewPiece)
            presetCells = presetCells + preset
        }
    }

    override val all get() = presetCells + cellAdd
    override var isSelectMode: Boolean = false
    override val selectedCount: Int get() = presetCells.count { it.isSelected }

    override fun removeSelected() {
        val presetCells = presetCells
        this.presetCells = presetCells.filter { !it.isSelected }
        isSelectMode = false
        deletePresets(presetCells.filter { it.isSelected })
    }

    override fun cancelSelectionMode() {
        for (cell in presetCells) {
            cell.isSelected = false
        }
        isSelectMode = false
    }

    private fun image(worldViewPiece: WorldView.Piece) = InputSource {
        val stream = ByteArrayOutputStream()
        worldViewPiece
            .takeScreenshot()
            .downsizeInteger(Pos2(presetImageMaxSizePx, presetImageMaxSizePx))
            .compress(Bitmap.CompressFormat.JPEG, presetImageQuality, stream)
        ByteArrayInputStream(stream.toByteArray())
    }

    private fun deletePresets(presets: List<PresetCellPresetImpl>) {
        scope.safeLaunch {
            repository.delete(presets.map { it.domain.await() })
        }
    }

    private fun addPreset(worldViewPiece: WorldView.Piece): PresetCellPresetImpl {
        val deferred = CompletableDeferred<DomainPreset>()
        val config = world.config
        scope.safeLaunch {
            val preset = repository.add(image(worldViewPiece), config)
            deferred.complete(preset)
        }
        return PresetCellPresetImpl(deferred.asAsync())
    }

    private inner class PresetCellPresetImpl(val domain: Async<DomainPreset>) : PresetCell.Preset() {
        private val selection = Selection()

        override val preset = Async {
            coroutineScope {
                val domain = domain.await()
                val configDeferred = safeAsync { domain.config.await() }
                val imageDeferred = safeAsync { loadBitmap(domain.imageJPG) }

                PresetPresentationImpl(
                    configDeferred.await() ?: Config(),
                    imageDeferred.await(),
                    selection
                )
            }
        }.cache()

        var isSelected: Boolean
            get() = selection.isSelected
            set(value) {
                selection.isSelected = value
            }
    }

    private inner class PresetPresentationImpl(
        val config: Config,
        override val image: Bitmap?,
        val selection: Selection
    ) : PresetPresentation {
        override val isSelected: Boolean get() = selection.isSelected

        override fun onClick() {
            if (isSelectMode) {
                selection.isSelected = !selection.isSelected
            } else {
                apply()
            }
        }

        override fun onLongClick() {
            isSelectMode = true
            selection.isSelected = !selection.isSelected
        }

        private fun apply() {
            world.config = Config(
                position = world.config.position,
                performance = world.config.performance,
                world = config.world,
                color = config.color
            )
            navigation.go(Place.PresetPreview)
        }
    }

    @Model
    private class Selection {
        var isSelected: Boolean = false
    }
}
