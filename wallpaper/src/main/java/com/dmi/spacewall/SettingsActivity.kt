package com.dmi.spacewall

import android.annotation.SuppressLint
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.view.ViewGroup.LayoutParams
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.*
import androidx.ui.core.setContent
import com.dmi.spacewall.gl.WorldRenderer
import com.dmi.spacewall.ui.SettingsRoot
import com.dmi.spacewall.ui.common.Ambients
import com.dmi.spacewall.ui.common.Place
import com.dmi.spacewall.ui.common.PlaceNavigation
import com.dmi.spacewall.ui.common.PresentationAmbients
import com.dmi.util.ui.ActivityAmbient
import com.dmi.util.ui.Navigation

class SettingsActivity : AppCompatActivity() {
    private lateinit var worldRenderer: WorldRenderer
    private lateinit var glSurfaceView: GLSurfaceView

    private val navigation: PlaceNavigation = Navigation(Place.Settings(), ::finish)

    @SuppressLint("ClickableViewAccessibility", "ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        worldRenderer = WorldRenderer(baseContext)
        worldRenderer.setConfig(app.domain.world.config)

        glSurfaceView = GLSurfaceView(this).apply {
            setEGLContextClientVersion(2)
            setRenderer(worldRenderer)
            layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        }

        setContentView(FrameLayout(this).apply {
            id = 1
            addView(glSurfaceView)
            addView(FrameLayout(this@SettingsActivity).apply {
                id = 2
                setContent(Recomposer.current()) {
                    content()
                }
            })
        })
    }

    @Composable
    private fun content() {
        val presentation by state { app.presentation(navigation) }
        Providers(
            ActivityAmbient provides this@SettingsActivity,
            Ambients.Navigation provides navigation,
            Ambients.App provides app,
            Ambients.WorldView provides worldRenderer,
            PresentationAmbients.App provides presentation
        ) {
            SettingsRoot()
        }

        Observe {
            worldRenderer.setConfig(app.domain.world.config)
        }
    }

    override fun onPause() {
        super.onPause()
        glSurfaceView.onPause()
    }

    override fun onResume() {
        glSurfaceView.onResume()
        super.onResume()
    }

    override fun onBackPressed() {
        navigation.exit()
    }
}
