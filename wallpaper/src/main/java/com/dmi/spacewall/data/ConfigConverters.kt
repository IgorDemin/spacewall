package com.dmi.spacewall.data

import com.dmi.spacewall.domain.Config
import ru.wildberries.data.db.entity.ConfigEntity

fun ConfigEntity.toDomain() = Config(
    position = Config.Position(
        tile = tile,
        velocitySpeed = velocitySpeed,
        velocityAngleDegree = velocityAngleDegree
    ),
    performance = Config.Performance(
        stepsFloat = stepsFloat,
        mainIsAntialiasing = mainIsAntialiasing,
        mainResolution = mainResolution,
        starsResolution = starsResolution
    ),
    world = Config.World(
        iterations = iterations,
        param1 = param1,
        param2 = param2,
        param3 = param3,
        param4 = param4
    ),
    color = Config.Color(
        backgroundHSV = backgroundHSV,
        gamma = gamma,
        intensityPow = intensityPow,
        hue = hue,
        saturation = saturation,
        value = value,
        alpha = alpha
    )
)

fun Config.toData(id: Long) = ConfigEntity(
    id = id,
    tile = position.tile,
    velocitySpeed = position.velocitySpeed,
    velocityAngleDegree = position.velocityAngleDegree,
    stepsFloat = performance.stepsFloat,
    mainIsAntialiasing = performance.mainIsAntialiasing,
    mainResolution = performance.mainResolution,
    starsResolution = performance.starsResolution,
    iterations = world.iterations,
    param1 = world.param1,
    param2 = world.param2,
    param3 = world.param3,
    param4 = world.param4,
    backgroundHSV = color.backgroundHSV,
    gamma = color.gamma,
    intensityPow = color.intensityPow,
    hue = color.hue,
    saturation = color.saturation,
    value = color.value,
    alpha = color.alpha
)
