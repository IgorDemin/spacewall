package com.dmi.spacewall.data

import androidx.room.withTransaction
import com.dmi.spacewall.AppData
import com.dmi.spacewall.domain.Config
import com.dmi.spacewall.domain.Preset
import com.dmi.spacewall.domain.Presets
import com.dmi.util.async.Async
import com.dmi.util.io.InputSource
import com.dmi.util.io.readBytes
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import ru.wildberries.data.db.AppDatabase
import ru.wildberries.data.db.entity.ImageEntity
import ru.wildberries.data.db.entity.PresetEntity
import java.io.ByteArrayInputStream
import kotlin.time.ExperimentalTime

fun PresetRepository(data: AppData) = PresetRepository(data.database)

@OptIn(ExperimentalTime::class, ExperimentalCoroutinesApi::class, FlowPreview::class)
class PresetRepository(private val db: AppDatabase) {
    val all: Async<Presets> = Async {
        db.presetDao().all().map(::toDomain)
    }

    suspend fun add(imageJPG: InputSource, config: Config): Preset = db.withTransaction {
        val configId = db.configDao().put(config.toData(id = 0))
        val imageId = db.imageDao().put(ImageEntity(imageJPG = imageJPG.readBytes()))
        val entity = PresetEntity(configId = configId, imageId = imageId)
        val id = db.presetDao().put(entity)
        toDomain(entity.copy(id = id))
    }

    suspend fun delete(presets: List<Preset>) {
        val ids = presets.filterIsInstance<DBPreset>().map { it.id }
        db.presetDao().delete(ids)
    }

    private fun toDomain(entity: PresetEntity) = DBPreset(
        entity.id,
        image(entity.imageId),
        config = Async {
            db.configDao().get(entity.configId)?.toDomain() ?: Config()
        }
    )

    private fun image(id: Long) = InputSource {
        val imageEntity = db.imageDao().get(id)
        ByteArrayInputStream(imageEntity?.imageJPG!!)
    }

    private class DBPreset(
        val id: Long,
        override val imageJPG: InputSource,
        override val config: Async<Config>
    ) : Preset
}
