package com.dmi.spacewall.data

import androidx.room.withTransaction
import com.dmi.spacewall.AppData
import com.dmi.spacewall.common.Analytics
import com.dmi.spacewall.domain.Config
import com.dmi.util.coroutine.CommandFlow
import com.dmi.util.coroutine.RootCoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.runBlocking
import ru.wildberries.data.db.AppDatabase
import ru.wildberries.data.db.entity.SettingsEntity
import kotlin.time.ExperimentalTime
import kotlin.time.milliseconds

fun SettingsRepository(data: AppData) = SettingsRepository(data.database, data.app.analytics)

@OptIn(ExperimentalTime::class, ExperimentalCoroutinesApi::class, FlowPreview::class)
class SettingsRepository(private val db: AppDatabase, private val analytics: Analytics) {
    private val scope = RootCoroutineScope()
    private val saveFlow = CommandFlow<Config>()

    var config: Config = readConfigBlocking()
        set(value) {
            field = value
            saveFlow.offer(value)
        }

    init {
        saveFlow
            .debounce(100.milliseconds)
            .onEach { saveConfig(it) }
            .launchIn(scope)
    }

    private fun readConfigBlocking() = runBlocking { readConfig() }

    private suspend fun readConfig() = try {
        db.withTransaction {
            val settings = db.settingsDao().get()
            db.configDao().get(settings?.configId ?: 0)?.toDomain()!!
        }
    } catch (e: Exception) {
        analytics.logException(e)
        Config()
    }

    private suspend fun saveConfig(config: Config) = try {
        db.withTransaction {
            val settings = db.settingsDao().get() ?: SettingsEntity()
            val configEntityId = db.configDao().put(config.toData(settings.configId))
            db.settingsDao().set(settings.copy(configId = configEntityId))
        }
    } catch (e: Exception) {
        analytics.logException(e)
    }
}