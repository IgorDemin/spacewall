package com.dmi.spacewall.common

import android.util.Log

class Analytics {
    fun logException(e: Exception) {
        Log.e("Analytics", "Exception", e)
    }
}
