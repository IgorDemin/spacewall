package com.dmi.spacewall.common

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

interface SafeAsync {
    val analytics: Analytics

    fun CoroutineScope.safeLaunch(action: suspend () -> Unit) = launch {
        try {
            action()
        } catch (e: Exception) {
            analytics.logException(e)
        }
    }

    fun <T : Any> CoroutineScope.safeAsync(action: suspend () -> T) = async {
        try {
            action()
        } catch (e: Exception) {
            analytics.logException(e)
            null
        }
    }
}
