package com.dmi.spacewall

import android.content.Context
import com.dmi.spacewall.common.Analytics
import com.dmi.spacewall.data.PresetRepository
import com.dmi.spacewall.data.SettingsRepository
import com.dmi.spacewall.domain.World
import com.dmi.spacewall.presentation.SettingsPresentation
import com.dmi.spacewall.ui.common.PlaceNavigation
import com.dmi.util.inject
import com.dmi.util.injectFactory
import ru.wildberries.data.db.AppDatabase

class App(val context: Context) {
    val analytics by lazy(::Analytics)
    val data by inject(::AppData)
    val domain by inject(::AppDomain)
    fun presentation(navigation: PlaceNavigation) = AppPresentation(this, navigation)
}

class AppData(val app: App) {
    val database by lazy { AppDatabase(app.context) }
    val settingsRepository by inject(::SettingsRepository)
    val presetRepository by inject(::PresetRepository)
}

class AppDomain(val app: App) {
    val world by inject(::World)
}

class AppPresentation(val app: App, val navigation: PlaceNavigation) {
    val settings = injectFactory(::SettingsPresentation)
}
