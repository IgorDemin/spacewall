package com.dmi.spacewall.domain

import com.dmi.util.async.Async
import com.dmi.util.io.InputSource

typealias Presets = List<Preset>

interface Preset {
    val imageJPG: InputSource
    val config: Async<Config>
}
