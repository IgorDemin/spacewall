package com.dmi.spacewall.domain

import androidx.compose.Model
import com.dmi.spacewall.AppDomain
import com.dmi.spacewall.data.SettingsRepository

fun World(domain: AppDomain) = World(domain.app.data.settingsRepository, domain.app.data.settingsRepository.config)

@Model
class World(
    private val settingsRepository: SettingsRepository,
    config: Config
) {
    var config: Config = config
        set(value) {
            field = value
            settingsRepository.config = value
        }

    fun reset() {
        config = Config()
    }
}
