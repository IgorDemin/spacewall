package com.dmi.spacewall.domain

import arrow.optics.optics
import com.dmi.util.*
import kotlin.math.*

@optics
data class Config(
    val position: Position = Position(),
    val performance: Performance = Performance(),
    val world: World = World(),
    val color: Color = Color()
) {
    @optics
    data class Position(
        val initialZ: Float = -2F,
        val zoom: Float = 0.8F,
        val tile: Float = 0.850F,
        val worldAngleRadians: Vec2 = Vec2(1F, 2F),
        val velocitySpeed: Float = 0.02F,
        val velocityAngleDegree: Float = 45F
    ) {
        val velocity by lazy {
            velocitySpeed * Vec2(
                cos(velocityAngleDegree.degreeToRadians()),
                sin(velocityAngleDegree.degreeToRadians())
            )
        }

        companion object
    }

    @optics
    data class Performance(
        val prerenderPart: Float = 0.05F,
        val stepsFloat: Float = 15F,
        val stepSize: Float = 0.05F,
        val mainIsAntialiasing: Boolean = true,
        val mainResolution: Float = 0.5F,
        val starsIsAntialiasing: Boolean = true,
        val starsResolution: FloatPair = FloatPair(1F, 0.5F)
    ) {
        val steps = stepsFloat.roundToInt()

        companion object
    }

    @optics
    data class World(
        val iterations: FloatPair = FloatPair(17F, 17F),
        val param1: FloatPair = FloatPair(1.3F, 1.3F),
        val param2: FloatPair = FloatPair(0.5F, 0.5F),
        val param3: FloatPair = FloatPair(1F, 1F),
        val param4: FloatPair = FloatPair(1F, 1F)
    ) {
        companion object
    }

    @optics
    data class Color(
        val backgroundHSV: Vec3 = Vec3(0F, 0F, 0F),
        val gamma: FloatPair = FloatPair(3F, 3F),
        val intensityPow: FloatPair = FloatPair(
            0.000001F,
            0.000001F
        ),
        val hue: FloatPair = FloatPair(0.05F, 0.05F),
        val saturation: FloatPair = FloatPair(1.0F, -1.0F),
        val value: FloatPair = FloatPair(1F, 1F),
        val alpha: FloatPair = FloatPair(1F, 1F)
    ) {
        val backgroundRGB by lazy { hsvToRgb(backgroundHSV) }

        companion object
    }

    fun forPlane(percent: Float) = Plane(
        starsResolution = performance.starsResolution.percent(percent),
        iterations = world.iterations.percent(percent),
        param1 = world.param1.percent(percent),
        param2 = world.param2.percent(percent),
        param3 = world.param3.percent(percent),
        param4 = world.param4.percent(percent),
        gamma = color.gamma.percent(percent),
        intensityPow = color.intensityPow.percent(percent),
        hue = color.hue.percent(percent),
        saturation = color.saturation.percent(percent),
        value = color.value.percent(percent),
        alpha = color.alpha.percent(percent)
    )

    data class Plane(
        val starsResolution: Float,
        val iterations: Float,
        val param1: Float,
        val param2: Float,
        val param3: Float,
        val param4: Float,
        val gamma: Float,
        val intensityPow: Float,

        val hue: Float,
        val saturation: Float,
        val value: Float,
        val alpha: Float
    ) {
        val intensity = 0.000001F * 10F.pow(intensityPow)

        val color: Vec4 = run {
            val hue = if (saturation >= 0.0) hue else (hue + 0.5F) % 1F
            Vec4(hsvToRgb(hue, abs(saturation), value), alpha)
        }
    }

    companion object
}
