package com.dmi.spacewall

import android.app.Application
import android.content.Context
import androidx.compose.FrameManager
import com.dmi.util.initOnce

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        FrameManager.ensureStarted()
        app = App(this)
    }

    var app: App by initOnce()
}

val Context.mainApplication get() = applicationContext as MainApplication
val Context.app get() = mainApplication.app
