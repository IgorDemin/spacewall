package com.dmi.spacewall.gl.star

import android.content.Context
import android.opengl.GLES20.*
import com.dmi.spacewall.R
import com.dmi.spacewall.domain.Config
import com.dmi.util.Disposable
import com.dmi.util.gl.GLArrayBuffer
import com.dmi.util.gl.GLProgram
import com.dmi.util.gl.bind
import com.dmi.util.gl.obj.Projection2D
import kotlin.math.roundToInt

fun StarPlaneProgram(context: Context) = GLProgram(context, R.raw.stars_vertex, R.raw.stars_fragment)

class StarPlane(
    private val program: GLProgram
) : Disposable {
    private val handles = object {
        val iterations = glGetUniformLocation(program.id, "iterations")
        val param1 = glGetUniformLocation(program.id, "param1")
        val param2 = glGetUniformLocation(program.id, "param2")
        val param3 = glGetUniformLocation(program.id, "param3")
        val param4 = glGetUniformLocation(program.id, "param4")
        val tile = glGetUniformLocation(program.id, "tile")
        val gamma = glGetUniformLocation(program.id, "gamma")
        val intensity = glGetUniformLocation(program.id, "intensity")
        val angle = glGetUniformLocation(program.id, "angle")

        val vertexPosition = glGetAttribLocation(program.id, "vertexPosition")
        val projectionOffset = glGetUniformLocation(program.id, "projectionOffset")
        val projectionScale = glGetUniformLocation(program.id, "projectionScale")
        val windowOffset = glGetUniformLocation(program.id, "windowOffset")
        val windowSize = glGetUniformLocation(program.id, "windowSize")
    }

    private val vertexPositionLength = 2

    private val vertexPositions = GLArrayBuffer(
        GL_STATIC_DRAW,
        listOf(
            -1F, -1F,
            1F, -1F,
            -1F, 1F,
            1F, 1F
        ).toFloatArray()
    )

    override fun dispose() {
        vertexPositions.dispose()
        program.dispose()
    }

    fun draw(window: StarWindow, projection: Projection2D, config: Config, planeConfig: Config.Plane) = program.bind {
        glEnableVertexAttribArray(handles.vertexPosition)
        vertexPositions.bind {
            glVertexAttribPointer(handles.vertexPosition, vertexPositionLength, GL_FLOAT, false, 0, 0)
        }

        glUniform1i(handles.iterations, planeConfig.iterations.roundToInt())
        glUniform1f(handles.param1, planeConfig.param1)
        glUniform1f(handles.param2, planeConfig.param2)
        glUniform1f(handles.param3, planeConfig.param3)
        glUniform1f(handles.param4, planeConfig.param4)
        glUniform1f(handles.tile, config.position.tile)
        glUniform1f(handles.gamma, planeConfig.gamma)
        glUniform1f(handles.intensity, planeConfig.intensity)
        glUniform2f(handles.angle, config.position.worldAngleRadians.x, config.position.worldAngleRadians.y)

        glUniform2f(handles.projectionOffset, projection.offset.x, projection.offset.y)
        glUniform1f(handles.projectionScale, projection.scale)
        glUniform3f(handles.windowOffset, window.offset.x, window.offset.y, window.offset.z)
        glUniform2f(handles.windowSize, window.size.x, window.size.y)

        glDrawArrays(GL_TRIANGLE_STRIP, 0, vertexPositions.size / vertexPositionLength)
    }
}