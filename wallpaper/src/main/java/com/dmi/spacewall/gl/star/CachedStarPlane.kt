package com.dmi.spacewall.gl.star

import android.content.Context
import com.dmi.spacewall.domain.Config
import com.dmi.util.*
import com.dmi.util.gl.GLTexture
import com.dmi.util.gl.infinityAtlas.IdentityAtlasModel
import com.dmi.util.gl.infinityAtlas.InfiniteAtlas
import com.dmi.util.gl.obj.AlphaQuad
import com.dmi.util.gl.openGLVersion
import kotlin.math.abs

class CachedStarPlane(
    context: Context,
    private val starPlane: StarPlane,
    size: Pos2,
    quad: AlphaQuad,
    private val config: Config,
    private val planeConfig: Config.Plane,
    private val z: Float
) : Disposable {
    private val prerenderPart = config.performance.prerenderPart
    private val atlasScale = 1 + 2 * prerenderPart
    private val worldSize = Vec2(2F, 2F)

    private val windows = StarWindows(size, config)
    private val atlas = InfiniteAtlas(
        quad,
        if (context.openGLVersion < 0x00030000) GLTexture.ColorFormat.LUMINANCE else GLTexture.ColorFormat.RED,
        if (config.performance.starsIsAntialiasing) GLTexture.Filter.LINEAR else GLTexture.Filter.NEAREST,
        IdentityAtlasModel((size * planeConfig.starsResolution).toPos(), worldSize).extended(atlasScale),
        color = planeConfig.color
    )
    private var atlasWindow: StarWindow? = null

    override fun dispose() {
        atlas.dispose()
    }

    fun draw(position: Vec2) {
        val window = windows.of(Vec3(position.x, position.y, z))

        val atlasWindow = atlasWindow
        if (atlasWindow == null || needRerender(oldWindow = atlasWindow, newWindow = window)) {
            val regionsForRender = regionsForRender(oldWindow = atlasWindow, newWindow = window)

            atlas.refresh(window.screenOffset, regionsForRender) {
                starPlane.draw(window, it, config, planeConfig)
            }

            this.atlasWindow = window
        }

        atlas.draw(window.screenOffset)
    }

    private fun needRerender(oldWindow: StarWindow, newWindow: StarWindow): Boolean {
        val offset = newWindow.screenOffset - oldWindow.screenOffset
        val rerenderOffset = prerenderPart * worldSize
        return abs(offset.x) > rerenderOffset.x || abs(offset.y) > rerenderOffset.y
    }

    private fun regionsForRender(oldWindow: StarWindow?, newWindow: StarWindow): Region2Pair {
        val renderRegion = Region2.ONE * atlasScale * worldSize

        return if (oldWindow == null) {
            Region2Pair(renderRegion)
        } else {
            val offset = newWindow.screenOffset - oldWindow.screenOffset
            diffAfterOffset(renderRegion - offset, offset)
        }
    }
}
