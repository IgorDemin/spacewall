package com.dmi.spacewall.gl

import android.content.Context
import android.opengl.GLES20.*
import androidx.annotation.AnyThread
import com.dmi.spacewall.domain.Config
import com.dmi.spacewall.gl.star.Stars
import com.dmi.util.Disposable
import com.dmi.util.FPSMeter
import com.dmi.util.Pos2
import com.dmi.util.Vec2
import com.dmi.util.gl.glWithViewport
import kotlin.time.ExperimentalTime
import kotlin.time.seconds


@OptIn(ExperimentalTime::class)
class GLWorld(context: Context, private val size: Pos2) : Disposable {
    @Volatile
    private var config: Config? = null

    private var stars = Stars(context, size)
    private val fpsMeter = FPSMeter("Time. World frame", period = 5.seconds)
    private var previous = System.currentTimeMillis()

    init {
        glDisable(GL_DEPTH_TEST)
        glEnable(GL_BLEND)
        glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA)
    }

    override fun dispose() {
        stars.dispose()
    }

    fun draw() = glWithViewport(size) {
        val time = System.currentTimeMillis()

        stars.draw((time - previous).toFloat() / 1000F)
        previous = time

        fpsMeter.check()
    }

    @AnyThread
    fun setOffset(offset: Vec2) {
        stars.setOffset(offset)
    }

    @AnyThread
    fun setConfig(config: Config?) {
        this.config = config
        stars.setConfig(config)
    }
}
