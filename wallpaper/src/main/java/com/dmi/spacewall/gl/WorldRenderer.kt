package com.dmi.spacewall.gl

import android.content.Context
import android.opengl.GLSurfaceView
import androidx.annotation.AnyThread
import com.dmi.spacewall.domain.Config
import com.dmi.spacewall.ui.WorldView
import com.dmi.util.Pos2
import com.dmi.util.Range2
import com.dmi.util.Vec2
import com.dmi.util.gl.GLScreenshots
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class WorldRenderer(private val context: Context) : GLSurfaceView.Renderer, WorldView {
    @Volatile
    private var config: Config? = null

    private var glWorld: GLWorld? = null
    private val screenshots = GLScreenshots()

    override fun onDrawFrame(gl: GL10?) {
        glWorld?.draw()
        screenshots.onDraw()
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        glWorld = GLWorld(context, Pos2(width, height))
        glWorld?.setConfig(config)
    }

    override fun onSurfaceCreated(gl: GL10?, eglConfig: EGLConfig?) = Unit

    @AnyThread
    override suspend fun takeScreenshot(range: Range2) = screenshots.take(range)

    @AnyThread
    fun setOffset(offset: Vec2) {
        glWorld?.setOffset(offset)
    }

    @AnyThread
    fun setConfig(config: Config) {
        if (this.config != config) {
            this.config = config
            glWorld?.setConfig(config)
        }
    }
}