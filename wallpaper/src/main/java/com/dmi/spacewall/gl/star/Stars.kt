package com.dmi.spacewall.gl.star

import android.content.Context
import android.opengl.GLES20.*
import androidx.annotation.AnyThread
import com.dmi.spacewall.domain.Config
import com.dmi.util.*
import com.dmi.util.gl.GLTexture
import com.dmi.util.gl.obj.AlphaQuad
import com.dmi.util.gl.obj.DrawableTexture
import com.dmi.util.gl.obj.Quad
import java.util.concurrent.atomic.AtomicBoolean

class Stars(private val context: Context, private val size: Pos2) : Disposable {
    @Volatile
    private var config: Config? = null

    @Volatile
    private var offset = Vec2.ZERO

    private val needRecreate = AtomicBoolean(false)
    private var position = Vec2.ZERO

    private val quad = AlphaQuad(context)
    private var starPlane =
        StarPlane(StarPlaneProgram(context))
    private var planes: List<CachedStarPlane> = emptyList()

    private val mainQuad = Quad(context)
    private var mainTexture: DrawableTexture? = null

    override fun dispose() {
        mainTexture?.dispose()
        mainQuad.dispose()

        planes.forEach(CachedStarPlane::dispose)
        starPlane.dispose()
        quad.dispose()
    }

    @AnyThread
    fun setOffset(offset: Vec2) {
        this.offset = offset
    }

    @AnyThread
    fun setConfig(config: Config?) {
        this.config = config
        needRecreate.set(true)
    }

    fun draw(dt: Float) {
        val config = config

        if (needRecreate.getAndSet(false)) {
            mainTexture?.dispose()
            planes.forEach(CachedStarPlane::dispose)
            planes = config?.let(::planes) ?: emptyList()
            mainTexture = config?.let(::mainTexture)
        }

        if (config != null) {
            performDraw(config, dt)
        }
    }

    private fun performDraw(config: Config, dt: Float) {
        position += config.position.velocity * dt

        val mainTexture = mainTexture
        if (mainTexture != null) {
            glClearColor(0F, 0F, 0F, 1f)
            glClear(GL_COLOR_BUFFER_BIT)

            mainTexture.refresh { drawWorld(config) }
            mainTexture.draw()
        } else {
            drawWorld(config)
        }
    }

    private fun drawWorld(config: Config) {
        glClearColor(config.color.backgroundRGB.x, config.color.backgroundRGB.y, config.color.backgroundRGB.z, 1f)
        glClear(GL_COLOR_BUFFER_BIT)
        planes.asReversed().forEach {
            it.draw(position + offset)
        }
    }

    private fun planes(config: Config) = (0 until config.performance.steps).map { step ->
        val percent = if (config.performance.steps > 1) step / (config.performance.steps - 1F) else 0F
        val planeConfig = config.forPlane(percent)
        CachedStarPlane(
            context,
            starPlane,
            (size * config.performance.mainResolution).toPos(),
            quad,
            config,
            planeConfig,
            z = 0.1F + step * config.performance.stepSize
        )
    }

    private fun mainTexture(config: Config) = if (config.performance.mainResolution != 1F) {
        val texture = GLTexture(
            size = (size * config.performance.mainResolution).toPos(),
            filter = if (config.performance.mainIsAntialiasing) GLTexture.Filter.LINEAR else GLTexture.Filter.NEAREST
        )
        DrawableTexture(texture, mainQuad)
    } else {
        null
    }
}
