package com.dmi.spacewall.gl.star

import com.dmi.spacewall.domain.Config
import com.dmi.util.*

data class StarWindow(val size: Vec2, val offset: Vec3) {
    val screenOffset = Vec2(offset.x / size.x * 2, offset.y / size.y * 2)
}

data class StarWindows(
    private val size: Pos2,
    private val config: Config
) {
    fun of(position: Vec3): StarWindow {
        val size = config.position.zoom * size.toVec() / size.x.toFloat() * position.z
        val offset = Vec3(position.x, position.y, position.z + config.position.initialZ)
        return StarWindow(size, offset)
    }
}
