package com.dmi.spacewall

import android.content.Context
import android.opengl.GLSurfaceView
import android.service.wallpaper.WallpaperService
import android.view.SurfaceHolder
import android.view.ViewGroup.LayoutParams
import com.dmi.spacewall.gl.WorldRenderer
import com.dmi.util.Vec2

class MainWallpaperService : WallpaperService() {

    override fun onCreateEngine() = object : WallpaperService.Engine() {
        private lateinit var glSurfaceView: WallpaperGLSurfaceView

        private val renderer = WorldRenderer(baseContext)

        inner class WallpaperGLSurfaceView(context: Context) : GLSurfaceView(context) {
            override fun getHolder() = surfaceHolder
        }

        override fun onCreate(surfaceHolder: SurfaceHolder) {
            super.onCreate(surfaceHolder)

            glSurfaceView = WallpaperGLSurfaceView(this@MainWallpaperService)
            glSurfaceView.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
            glSurfaceView.setEGLContextClientVersion(2)
            glSurfaceView.setRenderer(renderer)
        }

        override fun onVisibilityChanged(visible: Boolean) {
            super.onVisibilityChanged(visible)
            if (visible) {
                glSurfaceView.onResume()
                renderer.setConfig(app.domain.world.config)
            } else {
                glSurfaceView.onPause()
            }
        }

        override fun onOffsetsChanged(
            xOffset: Float,
            yOffset: Float,
            xOffsetStep: Float,
            yOffsetStep: Float,
            xPixelOffset: Int,
            yPixelOffset: Int
        ) {
            renderer.setOffset(Vec2(xOffset / 10F, 0F))
        }
    }
}
