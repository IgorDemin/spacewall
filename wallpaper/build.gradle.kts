@file:Suppress("UnstableApiUsage")

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
}

val config = loadProperties(".sign/sign.properties")
val signEnabled = (config["sign.enabled"] as String?)?.toBoolean() ?: false

android {
    buildFeatures {
        compose = true
    }

    kotlinOptions {
        jvmTarget = "1.8"
        freeCompilerArgs = listOf(
            "-Xopt-in=kotlin.RequiresOptIn"
        )
    }

    composeOptions {
        kotlinCompilerVersion = composeKotlinCompilerVersion
        kotlinCompilerExtensionVersion = composeVersion
    }

    if (signEnabled) {
        signingConfigs {
            create("main") {
                storeFile = File(rootProject.projectDir, ".sign/" + config["sign.store.file"] as String)
                storePassword = config["sign.store.password"] as String
                keyAlias = config["sign.key.alias"] as String
                keyPassword = config["sign.key.password"] as String
            }
        }
    }

    buildTypes {
        named("release") {
            signingConfig = if (signEnabled) signingConfigs["main"] else null
        }
        named("debug") {
            signingConfig = if (signEnabled) signingConfigs["main"] else null
        }
    }
}

dependencies {
    api("androidx.appcompat:appcompat:1.1.0")

    api("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutinesVersion")
    api("androidx.compose:compose-runtime:$composeVersion")
    api("androidx.ui:ui-framework:$composeVersion")
    api("androidx.ui:ui-layout:$composeVersion")
    api("androidx.ui:ui-material:$composeVersion")
    api("androidx.ui:ui-foundation:$composeVersion")
    api("androidx.ui:ui-animation:$composeVersion")
    api("androidx.ui:ui-tooling:$composeVersion")
    kapt("io.arrow-kt:arrow-meta:$arrowVersion")

    implementation(project(":util:android"))
    implementation(project(":db"))
    testImplementation(project(":util:commonTest"))
}
