package com.dmi.util.io

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.InputStream

fun InputSource(open: suspend () -> InputStream) = object : InputSource {
    override suspend fun open() = open()
}

interface InputSource {
    suspend fun open(): InputStream
}

suspend fun InputSource.readBytes() = open().use {
    withContext(Dispatchers.IO) {
        it.readBytes()
    }
}
