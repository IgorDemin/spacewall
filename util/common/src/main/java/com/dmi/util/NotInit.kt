package com.dmi.util

/**
 * Special object used for checking if value is init:
 *
 * var value: Any? = NotInit
 * if (value === NotInit) value = 4
 *
 * or
 *
 * var value: Any? = NotInit
 * if (value === NotInit) value = null
 */
internal object NotInit
