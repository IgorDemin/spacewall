package com.dmi.util

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun <T> threadLocal() = object : ReadWriteProperty<Any?, T> {
    private val value = ThreadLocal<T>()
    override fun getValue(thisRef: Any?, property: KProperty<*>): T = value.get() as T
    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) = this.value.set(value)
}

fun <T> threadLocal(initial: T) = object : ReadWriteProperty<Any?, T> {
    private val value = ThreadLocal<T>()
    override fun getValue(thisRef: Any?, property: KProperty<*>): T = value.get() ?: initial
    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) = this.value.set(value)
}

@Suppress("UNCHECKED_CAST")
fun <T> initOnce() = object : ReadWriteProperty<Any?, T> {
    private var value: Any? = NotInit

    override operator fun getValue(thisRef: Any?, property: KProperty<*>): T = value as T

    override operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        check(value !== NotInit) { "Cannot set value twice for ${property.name}" }
        this.value = value
    }
}
