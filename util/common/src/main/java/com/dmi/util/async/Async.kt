package com.dmi.util.async

import com.dmi.util.NotInit
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

fun <T> Async(await: suspend () -> T) = object : Async<T> {
    override suspend fun await() = await()
}

interface Async<out T> {
    suspend fun await(): T
}

class CacheAsync<T>(original: Async<T>) : Async<T> {
    private val mutex = Mutex()
    private var value: Any? = NotInit
    private var original: Async<T>? = original

    @Suppress("UNCHECKED_CAST")
    override suspend fun await(): T = mutex.withLock {
        if (value == NotInit) {
            value = original!!.await()
            original = null
        }
        value as T
    }
}

fun <T, R> Async<T>.map(transform: (T) -> R) = Async {
    transform(await())
}

fun <T> Async<T>.cache() = CacheAsync(this)

fun <T> T.asAsync() = Async { this }

fun <T> Deferred<T>.asAsync() = Async { await() }
