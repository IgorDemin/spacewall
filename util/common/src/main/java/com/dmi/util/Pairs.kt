package com.dmi.util

import arrow.optics.optics

@optics
data class FloatPair(val start: Float, val end: Float) {
    fun percent(percent: Float): Float = (1 - percent) * start + percent * end

    companion object
}
