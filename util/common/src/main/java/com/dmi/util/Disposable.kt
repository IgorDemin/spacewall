package com.dmi.util

interface Disposable {
    fun dispose()
}
