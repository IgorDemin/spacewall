package com.dmi.util

import arrow.optics.Lens
import arrow.optics.PLens

fun <S, A> PLens.Companion.immutable(default: A): Lens<S, A> = Lens(
    get = { default },
    set = { s: S, a: A -> s }
)
