package com.dmi.util

sealed class RangeSideF {
    abstract fun compareLeft(value: Float): Boolean

    abstract fun compareRight(value: Float): Boolean

    class Open(val value: Float) : RangeSideF() {
        override fun compareLeft(value: Float) = value > this.value

        override fun compareRight(value: Float) = value < this.value
    }

    class Closed(val value: Float) : RangeSideF() {
        override fun compareLeft(value: Float) = value >= this.value

        override fun compareRight(value: Float) = value <= this.value
    }
}

class RangeF(val left: RangeSideF, val right: RangeSideF) {
    operator fun contains(value: Float) = left.compareLeft(value) && right.compareRight(value)
}

fun open(value: Float) = RangeSideF.Open(value)

fun closed(value: Float) = RangeSideF.Closed(value)

operator fun RangeSideF.rangeTo(right: RangeSideF) = RangeF(this, right)
