package com.dmi.util

fun hsvToRgb(vec3: Vec3): Vec3 = hsvToRgb(vec3.x, vec3.y, vec3.z)

fun hsvToRgb(hue: Float, saturation: Float, value: Float): Vec3 {
    require(hue in 0F..1F)
    require(saturation in 0F..1F)
    require(value in 0F..1F)

    val h = (hue * 6).toInt()
    val f = hue * 6 - h
    val p = value * (1 - saturation)
    val q = value * (1 - f * saturation)
    val t = value * (1 - (1 - f) * saturation)
    return when (h) {
        0 -> Vec3(value, t, p)
        1 -> Vec3(q, value, p)
        2 -> Vec3(p, value, t)
        3 -> Vec3(p, q, value)
        4 -> Vec3(t, p, value)
        5 -> Vec3(value, p, q)
        6 -> Vec3(value, p, q)
        else -> throw IllegalStateException()
    }
}
