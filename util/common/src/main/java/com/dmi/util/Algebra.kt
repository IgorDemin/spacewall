package com.dmi.util

import arrow.optics.optics
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

@optics
data class Region2(val start: Vec2, val end: Vec2) {
    init {
        require(end.x >= start.x)
        require(end.y >= start.y)
    }

    val bottomLeft get() = Vec2(start.x, start.y)
    val bottomRight get() = Vec2(end.x, start.y)
    val topLeft get() = Vec2(start.x, end.y)
    val topRight get() = Vec2(end.x, end.y)
    val center get() = start + size / 2F

    val size get() = end - start
    val isEmpty get() = size.x == 0F && size.y == 0F

    companion object {
        val ONE = Vec2.ZERO..Vec2.ONE

        fun withCenter(center: Vec2, size: Vec2) = center - size / 2F..center + size / 2F
    }
}

data class Range2(val start: Pos2, val end: Pos2) {
    init {
        require(end.x >= start.x)
        require(end.y >= start.y)
    }

    val size get() = end - start
}

operator fun Vec2.rangeTo(other: Vec2) = Region2(this, other)
operator fun Pos2.rangeTo(other: Pos2) = Range2(this, other)

data class Region2Pair(val first: Region2? = null, val second: Region2? = null) {
    val isEmpty get() = first?.isEmpty == true && second?.isEmpty == true
}

fun diffAfterOffset(region: Region2, offset: Vec2): Region2Pair {
    val offsetRegion = region + offset
    val sizeX = min(region.size.x, abs(offset.x))
    val sizeY = min(region.size.y, abs(offset.y))

    val horizontalRegion = if (offset.y >= 0) {
        offsetRegion.topLeft - Vec2(0F, sizeY)..offsetRegion.topRight
    } else {
        offsetRegion.bottomLeft..offsetRegion.bottomRight + Vec2(0F, sizeY)
    }

    val verticalRegion = when {
        offset.x >= 0 && offset.y >= 0 -> offsetRegion.bottomRight - Vec2(sizeX, 0F)..horizontalRegion.bottomRight
        offset.x >= 0 && offset.y < 0 -> horizontalRegion.topRight - Vec2(sizeX, 0F)..offsetRegion.topRight
        offset.x < 0 && offset.y >= 0 -> offsetRegion.bottomLeft..horizontalRegion.bottomLeft + Vec2(sizeX, 0F)
        else -> horizontalRegion.topLeft..offsetRegion.topLeft + Vec2(sizeX, 0F)
    }

    return Region2Pair(horizontalRegion, verticalRegion)
}

@optics
data class Pos2(val x: Int, val y: Int) {
    companion object {
        val ZERO = Pos2(0, 0)
    }
}

@optics
data class Vec2(val x: Float, val y: Float) {
    companion object {
        val ZERO = Vec2(0F, 0F)
        val ONE = Vec2(1F, 1F)
    }
}

@optics
data class Vec3(val x: Float, val y: Float, val z: Float) {
    companion object
}

@optics
data class Vec4(val x: Float, val y: Float, val z: Float, val w: Float) {
    constructor(vec3: Vec3, w: Float) : this(vec3.x, vec3.y, vec3.z, w)

    companion object {
        val ZERO = Vec4(0F, 0F, 0F, 0F)
        val ONE = Vec4(1F, 1F, 1F, 1F)
    }
}

operator fun Vec2.unaryMinus() = Vec2(-x, -y)

operator fun Float.times(other: Vec2) = Vec2(this * other.x, this * other.y)
operator fun Pos2.times(other: Int) = Pos2(x * other, y * other)
operator fun Pos2.times(other: Float) = Vec2(x * other, y * other)
operator fun Pos2.times(other: Vec2) = Vec2(x * other.x, y * other.y)
operator fun Vec2.times(other: Float) = Vec2(x * other, y * other)
operator fun Vec2.times(other: Int) = Vec2(x * other, y * other)
operator fun Vec2.times(other: Vec2) = Vec2(x * other.x, y * other.x)
operator fun Vec2.times(other: Pos2) = Vec2(x * other.x, y * other.y)
operator fun Vec3.times(other: Float) = Vec3(x * other, y * other, z * other)
operator fun Region2.times(other: Float) = Region2(start * other, end * other)
operator fun Region2.times(other: Pos2) = Region2(start * other, end * other)
operator fun Region2.times(other: Vec2) = Region2(start * other, end * other)

operator fun Float.div(other: Vec2) = Vec2(this / other.x, this / other.y)
operator fun Vec2.div(other: Vec2) = Vec2(x / other.x, y / other.y)
operator fun Vec2.div(other: Float) = Vec2(x / other, y / other)
operator fun Pos2.div(other: Float) = Vec2(x / other, y / other)
operator fun Pos2.div(other: Int) = Pos2(x / other, y / other)
operator fun Vec3.div(other: Float) = Vec3(x / other, y / other, z / other)
operator fun Region2.div(other: Float) = Region2(start / other, end / other)
operator fun Region2.div(other: Vec2) = Region2(start / other, end / other)

operator fun Vec2.plus(other: Region2) = Region2(this + other.start, this + other.end)
operator fun Vec2.plus(other: Float) = Vec2(x + other, y + other)
operator fun Pos2.plus(other: Pos2) = Pos2(x + other.x, y + other.y)
operator fun Vec2.plus(other: Vec2) = Vec2(x + other.x, y + other.y)
operator fun Vec3.plus(other: Vec3) = Vec3(x + other.x, y + other.y, z + other.z)
operator fun Region2.plus(other: Vec2) = Region2(start + other, end + other)
operator fun Region2.plus(other: Float) = Region2(start + other, end + other)

operator fun Pos2.minus(other: Pos2) = Pos2(x - other.x, y - other.y)
operator fun Vec2.minus(other: Vec2) = Vec2(x - other.x, y - other.y)
operator fun Vec2.minus(other: Float) = Vec2(x - other, y - other)
operator fun Region2.minus(other: Float) = Region2(start - other, end - other)
operator fun Region2.minus(other: Vec2) = Region2(start - other, end - other)
operator fun Range2.minus(other: Pos2) = Range2(start - other, end - other)

infix fun Vec2.mod(other: Float) = Vec2(x mod other, y mod other)
infix fun Vec2.mod(other: Vec2) = Vec2(x mod other.x, y mod other.y)

infix fun Region2.intersection(other: Region2): Region2 {
    val intersectionStart = Vec2(max(start.x, other.start.x), max(start.y, other.start.y))
    val intersectionEnd = Vec2(min(end.x, other.end.x), min(end.y, other.end.y))
    return intersectionStart..Vec2(max(intersectionStart.x, intersectionEnd.x), max(intersectionStart.y, intersectionEnd.y))
}

fun Vec2.coerceIn(region: Region2) = Vec2(x.coerceIn(region.start.x..region.end.x), y.coerceIn(region.start.y..region.end.y))
fun Pos2.coerceIn(range: Range2) = Pos2(x.coerceIn(range.start.x..range.end.x), y.coerceIn(range.start.y..range.end.y))
fun Region2.coerceIn(region: Region2) = start.coerceIn(region)..end.coerceIn(region)
fun Range2.coerceIn(range: Range2) = start.coerceIn(range)..end.coerceIn(range)

fun Pos2.toVec() = Vec2(x.toFloat(), y.toFloat())
fun Vec2.toPos() = Pos2(x.toInt(), y.toInt())
fun Region2.toRange() = Range2(start.toPos(), (end + Vec2.ONE / 2F).toPos())
