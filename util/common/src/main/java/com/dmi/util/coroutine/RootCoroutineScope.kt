package com.dmi.util.coroutine

import kotlinx.coroutines.*
import java.lang.Thread.currentThread

private val uncaughtExceptionHandler = CoroutineExceptionHandler { coroutineContext, throwable ->
    val coroutineName = coroutineContext[CoroutineName]?.name
    val exception = RuntimeException("Unhandled coroutine exception from $coroutineName", throwable)
    currentThread().uncaughtExceptionHandler.uncaughtException(currentThread(), exception)
}

class RootCoroutineScope(coroutineName: String) : CoroutineScope {
    override val coroutineContext = Dispatchers.Main + SupervisorJob() + CoroutineName(coroutineName) + uncaughtExceptionHandler
}

@Suppress("FunctionName")
inline fun <reified T> T.RootCoroutineScope() = RootCoroutineScope(T::class.java.simpleName)
