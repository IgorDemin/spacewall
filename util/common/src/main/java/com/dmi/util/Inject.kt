package com.dmi.util

import com.dmi.util.async.Async

fun <Parent, Child> Parent.injectFactory(constructor: (Parent) -> Child): () -> Child = { constructor(this) }

fun <Parent, Child> Parent.injectAsync(constructor: suspend (Parent) -> Child) = Async { constructor(this) }

fun <Parent, Child> Parent.inject(constructor: (Parent) -> Child) = lazy { constructor(this) }
