package com.dmi.util

import kotlin.math.PI
import kotlin.math.floor

infix fun Float.mod(other: Float) = this - other * floor(this / other)

fun lerp(first: Float, second: Float, percent: Float) = (1 - percent) * first + percent * second

fun Float.degreeToRadians() = (this / 360 * 2 * PI).toFloat()
