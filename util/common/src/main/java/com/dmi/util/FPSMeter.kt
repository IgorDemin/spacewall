package com.dmi.util

import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.nanoseconds

@OptIn(ExperimentalTime::class)
class FPSMeter constructor(private val tag: String, private val period: Duration) {
    private var lastTime = System.nanoTime().nanoseconds
    private var i = 0

    private var lastFrameTime = System.nanoTime().nanoseconds
    private var maxFrameTime = Long.MIN_VALUE.nanoseconds

    fun check() {
        val time = System.nanoTime().nanoseconds
        i++
        maxFrameTime = maxOf(maxFrameTime, time - lastFrameTime)
        if (time >= lastTime + period) {
            val ms = (time - lastTime).inMilliseconds
            val maxFrameMs = maxFrameTime.inMilliseconds
            println("$tag average $ms ms, max $maxFrameMs ms")
            i = 0
            lastTime = time
            maxFrameTime = Long.MIN_VALUE.nanoseconds
        }
        lastFrameTime = time
    }
}
