package com.dmi.util.async

sealed class Progress {
    object Loading : Progress()
    class Loaded<T>(val value: T) : Progress()
}
