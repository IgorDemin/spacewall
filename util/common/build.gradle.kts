plugins {
    kotlin("jvm")
    kotlin("kapt")
}

dependencies {
    api("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlinVersion")
    api("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")

    api("io.arrow-kt:arrow-core:$arrowVersion")
    api("io.arrow-kt:arrow-syntax:$arrowVersion")
    api("io.arrow-kt:arrow-optics:$arrowVersion")
    kapt("io.arrow-kt:arrow-meta:$arrowVersion")

    testApi("org.junit.jupiter:junit-jupiter-engine:5.5.2")
    testApi("org.amshove.kluent:kluent:1.60")
}
