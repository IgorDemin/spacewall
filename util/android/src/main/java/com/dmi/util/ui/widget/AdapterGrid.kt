package com.dmi.util.ui.widget

import androidx.compose.Composable
import androidx.ui.core.Alignment
import androidx.ui.core.Modifier
import androidx.ui.foundation.Box
import androidx.ui.foundation.VerticalScroller
import androidx.ui.layout.Column
import androidx.ui.layout.Row
import androidx.ui.layout.fillMaxWidth

@Composable
fun <T> AdapterGrid(
    items: List<T>,
    columns: Int = 0,
    modifier: Modifier = Modifier,
    child: @Composable() (T) -> Unit
) = FakeAdapterList( // TODO replace to AdapterList when its scroll crash will be fixed
    items.chunked(columns),
    modifier = modifier
) { row ->
    Row(modifier = Modifier.fillMaxWidth()) {
        for (i in 0 until columns) {
            val item = row.getOrNull(i)
            Box(
                modifier = Modifier.weight(1F),
                gravity = Alignment.Center
            ) {
                if (item != null) child(item)
            }
        }
    }
}

@Composable
private fun <T> FakeAdapterList(items: List<T>, modifier: Modifier, child: @Composable() (T) -> Unit) {
    VerticalScroller(modifier = modifier) {
        Column {
            for (item in items) {
                child(item)
            }
        }
    }
}
