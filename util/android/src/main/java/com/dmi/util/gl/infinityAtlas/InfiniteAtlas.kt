package com.dmi.util.gl.infinityAtlas

import android.opengl.GLES10.*
import com.dmi.util.*
import com.dmi.util.gl.*
import com.dmi.util.gl.obj.AlphaQuad
import com.dmi.util.gl.obj.Projection2D

/**
 * Позволяет рисовать на бесконечном поле, переиспользуя всего одну текстуру.
 */
class InfiniteAtlas(
    private val quad: AlphaQuad,
    colorFormat: GLTexture.ColorFormat,
    filter: GLTexture.Filter,
    private val model: InfiniteAtlasModel,
    private val color: Vec4
) : Disposable {
    private val texture = GLTexture(model.textureSize, colorFormat, GLTexture.Wrap.REPEAT, filter)
    private val frameBuffer = GLFrameBuffer(texture)

    override fun dispose() {
        frameBuffer.dispose()
        texture.dispose()
    }

    fun refresh(offset: Vec2, regions: Region2Pair, draw: (Projection2D) -> Unit) = frameBuffer.bind {
        glWithViewport(texture.size) {
            val first = regions.first
            val second = regions.second

            if (first != null) performRefresh(offset, first, draw)
            if (second != null) performRefresh(offset, second, draw)
        }
    }

    private fun performRefresh(offset: Vec2, clip: Region2, draw: (Projection2D) -> Unit) {
        val pieces = model.piecesOf(offset, clip)
        pieces.leftBottom.refresh(draw)
        pieces.rightBottom.refresh(draw)
        pieces.leftTop.refresh(draw)
        pieces.rightTop.refresh(draw)
    }

    private fun InfiniteAtlasModel.Piece.refresh(draw: (Projection2D) -> Unit) {
        glClip(clip) {
            glClearColor(0F, 0F, 0F, 0F)
            glClear(GL_COLOR_BUFFER_BIT)
            draw(projection)
        }
    }

    fun draw(position: Vec2) {
        quad.draw(texture, model.textureRegionOf(position), color)
    }
}
