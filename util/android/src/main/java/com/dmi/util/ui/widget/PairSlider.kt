package com.dmi.util.ui.widget

import android.annotation.SuppressLint
import androidx.animation.*
import androidx.compose.Composable
import androidx.compose.remember
import androidx.compose.state
import androidx.ui.animation.asDisposableClock
import androidx.ui.core.*
import androidx.ui.core.gesture.pressIndicatorGestureFilter
import androidx.ui.foundation.Box
import androidx.ui.foundation.Canvas
import androidx.ui.foundation.Icon
import androidx.ui.foundation.animation.FlingConfig
import androidx.ui.foundation.animation.fling
import androidx.ui.foundation.gestures.DragDirection
import androidx.ui.foundation.gestures.draggable
import androidx.ui.foundation.shape.corner.CircleShape
import androidx.ui.geometry.Offset
import androidx.ui.graphics.Color
import androidx.ui.graphics.Paint
import androidx.ui.graphics.PaintingStyle
import androidx.ui.graphics.StrokeCap
import androidx.ui.layout.*
import androidx.ui.material.MaterialTheme
import androidx.ui.material.Slider
import androidx.ui.material.Surface
import androidx.ui.material.ripple.ripple
import androidx.ui.res.vectorResource
import androidx.ui.semantics.Semantics
import androidx.ui.semantics.accessibilityValue
import androidx.ui.unit.PxPosition
import androidx.ui.unit.dp
import androidx.ui.unit.px
import androidx.ui.unit.toRect
import com.dmi.util.R
import com.dmi.util.lerp
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

class RangeValue(val start: Float, val end: Float)

/**
 * Copy of [androidx.ui.material.Slider] with two slide thumbs
 */
@Composable
fun PairSlider(
    value: RangeValue,
    onValueChange: (RangeValue) -> Unit,
    modifier: Modifier = Modifier,
    valueRange: ClosedFloatingPointRange<Float> = 0f..1f,
    onValueChangeEnd: () -> Unit = {},
    color: Color = MaterialTheme.colors.primary
) {
    val onValueStartChange = { start: Float ->
        onValueChange(RangeValue(start, value.end))
    }

    val onValueEndChange = { end: Float ->
        onValueChange(RangeValue(value.start, end))
    }

    val clock = AnimationClockAmbient.current.asDisposableClock()

    val positionStart = remember(valueRange) {
        SliderPosition(value.start, valueRange, clock, onValueStartChange)
    }

    val positionEnd = remember(valueRange) {
        SliderPosition(value.end, valueRange, clock, onValueEndChange)
    }

    positionStart.onValueChange = onValueStartChange
    positionStart.scaledValue = value.start

    positionEnd.onValueChange = onValueEndChange
    positionEnd.scaledValue = value.end

    Semantics(container = true, mergeAllDescendants = true) {
        Box(modifier = modifier) {
            WithConstraints { constraints, _ ->
                val maxPx = constraints.maxWidth.value.toFloat()
                val minPx = 0f
                positionStart.setBounds(minPx, maxPx)
                positionEnd.setBounds(minPx, maxPx)

                val flingConfigStart = SliderFlingConfig(positionStart) { endValue ->
                    positionStart.holder.snapTo(endValue)
                    onValueChangeEnd()
                }
                val flingConfigEnd = SliderFlingConfig(positionEnd) { endValue ->
                    positionEnd.holder.snapTo(endValue)
                    onValueChangeEnd()
                }
                val startGestureEndAction = { velocity: Float ->
                    if (flingConfigStart != null) {
                        positionStart.holder.fling(flingConfigStart, velocity)
                    } else {
                        onValueChangeEnd()
                    }
                }
                val endGestureEndAction = { velocity: Float ->
                    if (flingConfigEnd != null) {
                        positionEnd.holder.fling(flingConfigEnd, velocity)
                    } else {
                        onValueChangeEnd()
                    }
                }
                val pressedStart = state { false }
                val pressedEnd = state { false }
                val isDraggingStart = state { false }

                fun updateDraggingPosition(startPos: PxPosition) {
                    val startPx = positionStart.holder.value
                    val endPx = positionEnd.holder.value
                    isDraggingStart.value = abs(startPos.x.value - startPx) <= abs(startPos.x.value - endPx)
                }

                fun draggingPosition() = if (isDraggingStart.value) positionStart else positionEnd

                fun draggingGestureEndAction(velocity: Float) = if (isDraggingStart.value) {
                    startGestureEndAction(velocity)
                } else {
                    endGestureEndAction(velocity)
                }

                fun draggingPressed() = if (isDraggingStart.value) pressedStart else pressedEnd

                val press = Modifier.pressIndicatorGestureFilter(
                    onStart = { pos ->
                        updateDraggingPosition(pos)
                        draggingPosition().holder.snapTo(pos.x.value)
                        draggingPressed().value = true
                    },
                    onStop = {
                        pressedStart.value = false
                        pressedEnd.value = false
                        draggingGestureEndAction(0f)
                    })

                val drag = Modifier.draggable(
                    dragDirection = DragDirection.Horizontal,
                    onDragDeltaConsumptionRequested = { delta ->
                        draggingPosition().holder.snapTo(draggingPosition().holder.value + delta)
                        // consume all so slider won't participate in nested scrolling
                        delta
                    },
                    onDragStarted = { pos ->
                        updateDraggingPosition(pos)
                        draggingPressed().value = true
                    },
                    onDragStopped = { velocity ->
                        pressedStart.value = false
                        pressedEnd.value = false
                        draggingGestureEndAction(velocity)
                    },
                    startDragImmediately = draggingPosition().holder.isRunning
                )
                val coercedStart = value.start.coerceIn(positionStart.startValue, positionStart.endValue)
                val coercedEnd = value.end.coerceIn(positionEnd.startValue, positionEnd.endValue)
                val fractionStart =
                    calcFraction(positionStart.startValue, positionStart.endValue, coercedStart)
                val fractionEnd =
                    calcFraction(positionEnd.startValue, positionEnd.endValue, coercedEnd)
                Semantics(container = true, properties = { accessibilityValue = "$coercedStart $coercedEnd" }) {
                    SliderImpl(
                        fractionStart, fractionEnd, color, maxPx, pressedStart.value, pressedEnd.value,
                        modifier = press.plus(drag)
                    )
                }
            }
        }
    }
}

@Composable
private fun SliderImpl(
    positionStart: Float,
    positionEnd: Float,
    color: Color,
    width: Float,
    pressedStart: Boolean,
    pressedEnd: Boolean,
    modifier: Modifier
) {
    val widthDp = with(DensityAmbient.current) {
        width.px.toDp()
    }
    Stack(modifier + DefaultSliderConstraints) {
        val thumbSize = ThumbRadius * 2
        val offsetStart = (widthDp - thumbSize) * positionStart
        val offsetEnd = (widthDp - thumbSize) * positionEnd
        val center = Modifier.gravity(Alignment.CenterStart)
        Track(center.fillMaxSize(), color, positionStart, positionEnd)
        Box(center.padding(start = offsetStart)) {
            Surface(
                shape = CircleShape,
                color = color,
                elevation = if (pressedStart) 6.dp else 1.dp,
                modifier = Modifier.ripple(bounded = false).preferredSize(thumbSize, thumbSize)
            ) {
                Icon(vectorResource(R.drawable.ic_baseline_arrow_right_24), modifier = Modifier.gravity(Alignment.Center))
            }
        }
        Box(center.padding(start = offsetEnd)) {
            Surface(
                shape = CircleShape,
                color = color,
                elevation = if (pressedEnd) 6.dp else 1.dp,
                modifier = Modifier.ripple(bounded = false).preferredSize(thumbSize, thumbSize)
            ) {
                Icon(vectorResource(R.drawable.ic_baseline_arrow_left_24), modifier = Modifier.gravity(Alignment.Center))
            }
        }
    }
}

@Composable
private fun Track(
    modifier: Modifier,
    color: Color,
    positionStart: Float,
    positionEnd: Float
) {
    val paint = remember {
        Paint().apply {
            this.isAntiAlias = true
            this.strokeCap = StrokeCap.round
            this.style = PaintingStyle.stroke
        }
    }
    Canvas(modifier) {
        paint.strokeWidth = TrackHeight.toPx().value
        val parentRect = size.toRect()
        val thumbPx = ThumbRadius.toPx().value
        val centerHeight = size.height.value / 2
        val sliderMin = Offset(parentRect.left + thumbPx, centerHeight)
        val sliderMax = Offset(parentRect.right - thumbPx, centerHeight)
        paint.color = color.copy(alpha = InactiveTrackColorAlpha)
        drawLine(sliderMin, sliderMax, paint)
        val sliderStart = Offset(
            sliderMin.dx + (sliderMax.dx - sliderMin.dx) * max(positionStart, positionEnd),
            centerHeight
        )
        val sliderEnd = Offset(
            sliderMin.dx + (sliderMax.dx - sliderMin.dx) * min(positionStart, positionEnd),
            centerHeight
        )
        paint.color = color
        drawLine(sliderStart, sliderEnd, paint)
    }
}

// Scale x1 from a1..b1 range to a2..b2 range
private fun scale(a1: Float, b1: Float, x1: Float, a2: Float, b2: Float) =
    lerp(a2, b2, calcFraction(a1, b1, x1))

// Calculate the 0..1 fraction that `pos` value represents between `a` and `b`
private fun calcFraction(a: Float, b: Float, pos: Float) =
    (if (b - a == 0f) 0f else (pos - a) / (b - a)).coerceIn(0f, 1f)

@Composable
private fun SliderFlingConfig(
    value: SliderPosition,
    anchors: List<Float> = emptyList(),
    onSuccessfulEnd: (Float) -> Unit
): FlingConfig? {
    if (anchors.isEmpty()) {
        return null
    } else {
        val adjustTarget: (Float) -> TargetAnimation? = { _ ->
            val now = value.holder.value
            val point = anchors.minBy { abs(it - now) }
            val adjusted = point ?: now
            TargetAnimation(adjusted, SliderToTickAnimation)
        }
        return FlingConfig(
            adjustTarget = adjustTarget,
            onAnimationEnd = { reason, endValue, _ ->
                if (reason != AnimationEndReason.Interrupted) {
                    onSuccessfulEnd(endValue)
                }
            }
        )
    }
}

/**
 * Internal state for [Slider] that represents the Slider value, its bounds and optional amount of
 * steps evenly distributed across the Slider range.
 *
 * @param initial initial value for the Slider when created. If outside of range provided,
 * initial position will be coerced to this range
 * @param valueRange range of values that Slider value can take
 * @param steps if greater than 0, specifies the amounts of discrete values, evenly distributed
 * between across the whole value range. If 0, slider will behave as a continuous slider and allow
 * to choose any value from the range specified
 */
private class SliderPosition(
    initial: Float = 0f,
    val valueRange: ClosedFloatingPointRange<Float> = 0f..1f,
    animatedClock: AnimationClockObservable,
    var onValueChange: (Float) -> Unit
) {

    internal val startValue: Float = valueRange.start
    internal val endValue: Float = valueRange.endInclusive

    internal var scaledValue: Float = initial
        set(value) {
            val scaled = scale(startValue, endValue, value, startPx, endPx)
            // floating point error due to rescaling
            if ((scaled - holder.value) > floatPointMistakeCorrection) {
                holder.snapTo(scaled)
            }
        }

    private val floatPointMistakeCorrection = (valueRange.endInclusive - valueRange.start) / 100

    private var endPx = Float.MAX_VALUE
    private var startPx = Float.MIN_VALUE

    internal fun setBounds(min: Float, max: Float) {
        if (startPx == min && endPx == max) return
        val newValue = scale(startPx, endPx, holder.value, min, max)
        startPx = min
        endPx = max
        holder.setBounds(min, max)
        holder.snapTo(newValue)
    }

    @SuppressLint("UnnecessaryLambdaCreation")
    internal val holder =
        CallbackBasedAnimatedFloat(
            scale(startValue, endValue, initial, startPx, endPx),
            animatedClock
        ) { onValueChange(scale(startPx, endPx, it, startValue, endValue)) }
}

private class CallbackBasedAnimatedFloat(
    initial: Float,
    clock: AnimationClockObservable,
    var onValue: (Float) -> Unit
) : AnimatedFloat(clock) {

    override var value = initial
        set(value) {
            onValue(value)
            field = value
        }
}

private val ThumbRadius = 10.dp
private val TrackHeight = 4.dp
private val SliderHeight = 48.dp
private val SliderMinWidth = 144.dp // TODO: clarify min width
private val DefaultSliderConstraints =
    Modifier.preferredWidthIn(minWidth = SliderMinWidth)
        .preferredHeightIn(maxHeight = SliderHeight)
private val InactiveTrackColorAlpha = 0.24f
private val SliderToTickAnimation = TweenBuilder<Float>().apply { duration = 100 }
