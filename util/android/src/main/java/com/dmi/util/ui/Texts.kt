package com.dmi.util.ui

import androidx.compose.Composable
import androidx.compose.Providers
import androidx.ui.foundation.ContentColorAmbient
import androidx.ui.foundation.ProvideTextStyle
import androidx.ui.foundation.currentTextStyle
import androidx.ui.graphics.Color

@Composable
fun withTextColor(
    color: Color,
    content: @Composable() () -> Unit
) = ProvideTextStyle(currentTextStyle().copy(color = color)) {
    Providers(
        ContentColorAmbient provides color,
        children = content
    )
}
