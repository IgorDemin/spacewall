package com.dmi.util.ui

import android.app.Activity
import androidx.compose.staticAmbientOf

val ActivityAmbient = staticAmbientOf<Activity>()