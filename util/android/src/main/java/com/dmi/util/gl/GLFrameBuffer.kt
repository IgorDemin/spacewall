package com.dmi.util.gl

import android.opengl.GLES20.*

@OptIn(ExperimentalStdlibApi::class)
class GLFrameBuffer(private val texture: GLTexture) : GLResource {
    private val id = glGenFrameBuffer()

    init {
        bind {
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.id, 0)
            if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
                val errorCode = glGetError()
                error("Framebuffer is not completed. Error code: $errorCode")
            }
        }
    }

    override fun dispose() {
        glDelFrameBuffer(id)
    }


    override fun bind() {
        glBindFramebuffer(GL_FRAMEBUFFER, id)
        bound.add(this)
    }

    override fun unbind() {
        bound.removeLast()
        glBindFramebuffer(GL_FRAMEBUFFER, bound.lastOrNull()?.id ?: 0)
    }

    companion object {
        private val bound = ArrayList<GLFrameBuffer>()
    }
}