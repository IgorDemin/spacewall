package com.dmi.util.gl.obj

import com.dmi.util.Disposable
import com.dmi.util.gl.GLFrameBuffer
import com.dmi.util.gl.GLTexture
import com.dmi.util.gl.bind
import com.dmi.util.gl.glWithViewport

class DrawableTexture(private val texture: GLTexture, private val quad: Quad) : Disposable {
    private val frameBuffer = GLFrameBuffer(texture)

    fun refresh(draw: () -> Unit) = frameBuffer.bind {
        glWithViewport(texture.size) {
            draw()
        }
    }

    fun draw() {
        quad.draw(texture)
    }

    override fun dispose() {
        frameBuffer.dispose()
        texture.dispose()
    }
}