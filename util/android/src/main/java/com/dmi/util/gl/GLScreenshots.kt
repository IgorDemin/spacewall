package com.dmi.util.gl

import android.graphics.Bitmap
import android.opengl.GLES20.*
import androidx.annotation.AnyThread
import com.dmi.util.Range2
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.nio.ByteBuffer
import java.nio.ByteOrder

class GLScreenshots {
    @Volatile
    private var readPixelsTask: ReadPixelsTask? = null

    fun onDraw() {
        readPixelsTask?.readPixels()
        readPixelsTask = null
    }

    @AnyThread
    private suspend fun readPixels(range: Range2, buffer: ByteBuffer) {
        var readPixelsTask = readPixelsTask
        readPixelsTask?.await()
        readPixelsTask = ReadPixelsTask(range, buffer)
        this.readPixelsTask = readPixelsTask
        readPixelsTask.await()
    }

    @AnyThread
    suspend fun take(range: Range2): Bitmap = withContext(Dispatchers.Default) {
        val screenshotSize: Int = range.size.x * range.size.y

        val buffer = ByteBuffer
            .allocateDirect(screenshotSize * BYTES_PER_INTEGER)
            .order(ByteOrder.nativeOrder())

        readPixels(range, buffer)

        val pixels = IntArray(screenshotSize)
        buffer.asIntBuffer().get(pixels)

        // Red and blue channels are swapped
        for (i in 0 until screenshotSize) {
            pixels[i] = pixels[i] and -0xff0100 or
                (pixels[i] and 0x000000ff shl 16) or
                (pixels[i] and 0x00ff0000 shr 16)
        }

        val bitmap: Bitmap = Bitmap.createBitmap(range.size.x, range.size.y, Bitmap.Config.ARGB_8888)
        bitmap.setPixels(pixels, screenshotSize - range.size.x, -range.size.x, 0, 0, range.size.x, range.size.y)
        bitmap
    }
}

private class ReadPixelsTask(private val range: Range2, private val buffer: ByteBuffer) {
    private var pixelsCompletable = CompletableDeferred<Unit>()

    fun readPixels() {
        glReadPixels(range.start.x, range.start.y, range.size.x, range.size.y, GL_RGBA, GL_UNSIGNED_BYTE, buffer)
        pixelsCompletable.complete(Unit)
    }

    suspend fun await() = pixelsCompletable.await()
}
