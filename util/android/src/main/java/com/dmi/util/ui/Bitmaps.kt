package com.dmi.util.ui

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.BitmapFactory.decodeStream
import com.dmi.util.Pos2
import com.dmi.util.async.Async
import com.dmi.util.div
import com.dmi.util.io.InputSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

fun InputSource.asAsyncBitmap(): Async<Bitmap> = Async { loadBitmap(this) }

suspend fun Bitmap.resize(size: Pos2, filter: Boolean): Bitmap = withContext(Dispatchers.Default) {
    Bitmap.createScaledBitmap(this@resize, size.x, size.y, filter)
}

/**
 * Scale down bitmap with integer scale without filtering
 */
suspend fun Bitmap.downsizeInteger(desiredSize: Pos2): Bitmap = withContext(Dispatchers.Default) {
    val size = Pos2(width, height)
    val sampleSize = calculateSampleSize(size, desiredSize)
    val newSize = size / sampleSize
    Bitmap.createScaledBitmap(this@downsizeInteger, newSize.x, newSize.y, false)
}

suspend fun loadBitmap(source: InputSource): Bitmap = withContext(Dispatchers.IO) {
    val bitmapOptions = BitmapFactory.Options()
    source.open().use {
        decodeStream(it, null, bitmapOptions)!!
    }
}

suspend fun loadBitmap(source: InputSource, desiredSize: Pos2): Bitmap = withContext(Dispatchers.IO) {
    val bitmapOptions = BitmapFactory.Options()
    bitmapOptions.inJustDecodeBounds = true
    source.open().use {
        decodeStream(it, null, bitmapOptions)!!
    }

    bitmapOptions.inJustDecodeBounds = false
    bitmapOptions.inSampleSize = calculateSampleSize(Pos2(bitmapOptions.outWidth, bitmapOptions.outHeight), desiredSize)
    source.open().use {
        decodeStream(it, null, bitmapOptions)!!
    }
}

fun calculateSampleSize(factSize: Pos2, desiredSize: Pos2): Int {
    var sampleSize = 1

    while (factSize.x / (sampleSize + 1) >= desiredSize.x && factSize.y / (sampleSize + 1) >= desiredSize.y) {
        sampleSize++
    }

    return sampleSize
}
