package com.dmi.util.ui

import androidx.compose.*
import androidx.ui.core.Modifier
import androidx.ui.layout.padding
import androidx.ui.layout.preferredSize
import androidx.ui.material.CircularProgressIndicator
import androidx.ui.unit.dp
import com.dmi.util.async.Async
import com.dmi.util.async.Progress
import com.dmi.util.coroutine.RootCoroutineScope
import kotlinx.coroutines.launch

val scope = RootCoroutineScope("ProgressView")
@Composable
fun MediumProgressIndicator() = CircularProgressIndicator(Modifier.preferredSize(50.dp).padding(4.dp))

@Suppress("UNCHECKED_CAST")
@Composable
fun <T> LoadingView(
    value: Async<T>,
    onLoading: @Composable() () -> Unit = { MediumProgressIndicator() },
    onLoaded: @Composable() (value: T) -> Unit
) {
    val loading: Loading<T> by stateFor(value) { Loading(value) }

    onDispose {
        loading.cancel()
    }

    when (val progress = loading.progress) {
        Progress.Loading -> onLoading()
        is Progress.Loaded<*> -> onLoaded(progress.value as T)
    }
}

@Model
private class Loading<T>(private val async: Async<T>) {
    var progress: Progress = Progress.Loading

    private val job = scope.launch {
        progress = Progress.Loaded(async.await())
    }

    fun cancel() {
        job.cancel()
    }
}
