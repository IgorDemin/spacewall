package com.dmi.util.gl

import com.dmi.util.Disposable

interface GLObject : Disposable {
    fun draw()
    override fun dispose()
}