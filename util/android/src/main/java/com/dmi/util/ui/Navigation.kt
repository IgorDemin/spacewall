package com.dmi.util.ui

import androidx.compose.Model

@Model
class Navigation<T> constructor(
    initialElement: T,
    private val _exit: () -> Unit = {}
) {
    var list = listOf(initialElement)
        private set

    fun go(element: T) {
        list = list + element
    }

    fun exit() {
        if (list.size <= 1) {
            _exit()
        } else {
            list = list.dropLast(1)
        }
    }
}
