package com.dmi.util.gl.infinityAtlas

import com.dmi.util.*
import com.dmi.util.gl.infinityAtlas.InfiniteAtlasModel.Piece
import com.dmi.util.gl.infinityAtlas.InfiniteAtlasModel.Pieces
import com.dmi.util.gl.obj.Projection2D

interface InfiniteAtlasModel {
    val textureSize: Pos2
    val worldSize: Vec2

    fun extended(scale: Float) = ExtendedAtlasModel(this, scale)

    fun piecesOf(position: Vec2, clip: Region2 = Vec2.ZERO..worldSize): Pieces

    fun textureRegionOf(position: Vec2): Region2

    data class Pieces(
        val leftBottom: Piece,
        val rightBottom: Piece,
        val leftTop: Piece,
        val rightTop: Piece
    )

    data class Piece(
        val projection: Projection2D,
        val clip: Region2
    )
}

class IdentityAtlasModel(override val textureSize: Pos2, override val worldSize: Vec2) : InfiniteAtlasModel {
    override fun piecesOf(position: Vec2, clip: Region2): Pieces {
        val projectionOffset = position mod worldSize
        val textureClip = (projectionOffset + clip) / worldSize
        return Pieces(
            leftBottom = Piece(
                projection = Projection2D((projectionOffset - worldSize), scale = 1F),
                clip = (textureClip - Vec2(1F, 1F)).coerceIn(Region2.ONE)
            ),
            rightBottom = Piece(
                projection = Projection2D(projectionOffset - Vec2(0F, worldSize.y), scale = 1F),
                clip = (textureClip - Vec2(0F, 1F)).coerceIn(Region2.ONE)
            ),
            leftTop = Piece(
                projection = Projection2D(projectionOffset - Vec2(worldSize.x, 0F), scale = 1F),
                clip = (textureClip - Vec2(1F, 0F)).coerceIn(Region2.ONE)
            ),
            rightTop = Piece(
                projection = Projection2D(projectionOffset - Vec2(0F, 0F), scale = 1F),
                clip = (textureClip - Vec2(0F, 0F)).coerceIn(Region2.ONE)
            )
        )
    }

    override fun textureRegionOf(position: Vec2): Region2 {
        val projectionOffset = position mod worldSize
        val textureOffset = projectionOffset / worldSize
        return textureOffset + Region2.ONE
    }
}

class ExtendedAtlasModel(
    original: InfiniteAtlasModel,
    private val scale: Float
) : InfiniteAtlasModel {
    override val textureSize = (original.textureSize * scale).toPos()
    override val worldSize = original.worldSize * scale
    private val scaled = IdentityAtlasModel(textureSize, worldSize)

    override fun piecesOf(position: Vec2, clip: Region2): Pieces {
        val pieces = scaled.piecesOf(position, clip)
        return Pieces(
            leftBottom = pieces.leftBottom.scale(scale),
            rightBottom = pieces.rightBottom.scale(scale),
            leftTop = pieces.leftTop.scale(scale),
            rightTop = pieces.rightTop.scale(scale)
        )
    }

    private fun Piece.scale(scale: Float) = Piece(
        Projection2D(projection.offset / scale, projection.scale / scale),
        clip = clip
    )

    override fun textureRegionOf(position: Vec2): Region2 {
        val region = scaled.textureRegionOf(position)
        return Region2.withCenter(
            center = region.center,
            size = region.size / scale
        )
    }
}
