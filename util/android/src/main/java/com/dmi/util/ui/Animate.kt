/**
 * Copy of https://github.com/zsoltk/compose-router/pull/11/files with some modifications
 */

package com.dmi.util.ui

import androidx.animation.*
import androidx.compose.Composable
import androidx.compose.Immutable
import androidx.compose.Recompose
import androidx.compose.Stable
import androidx.compose.key
import androidx.compose.remember
import androidx.ui.animation.Transition
import androidx.ui.core.Modifier
import androidx.ui.core.clip
import androidx.ui.core.drawWithContent
import androidx.ui.foundation.Box
import androidx.ui.graphics.RectangleShape
import androidx.ui.layout.Stack
import androidx.ui.layout.fillMaxHeight
import androidx.ui.layout.fillMaxWidth
import com.dmi.util.ui.AnimationParams.Opacity
import com.dmi.util.ui.AnimationParams.Rotation
import com.dmi.util.ui.AnimationParams.X
import com.dmi.util.ui.AnimationParams.Y
import com.dmi.util.ui.TransitionStates.*
import kotlin.math.roundToInt

@Composable
fun <T : Any> AnimateChange(
    current: T,
    transition: TransitionDefinition<TransitionStates>,
    children: @Composable() (T) -> Unit
) {
    val animState = remember { AnimationState<T>() }
    val transitionDefinition = remember {
        transition.fillDefault()
    }
    if (animState.current != current) {
        animState.current = current
        val keys = animState.items.mapNotNull { it.key.takeIf { it != current } }
        animState.items.clear()
        keys.mapTo(animState.items) { key ->
            AnimationItem(key) { recompose, children ->
                ExitTransition(transitionDefinition, children) {
                    if (it === Exit && animState.current == current) {
                        animState.items.removeAll { it.key == key }
                        recompose()
                    }
                }
            }
        }

        val isFirst = keys.isEmpty()
        animState.items += AnimationItem(current) { _, children ->
            EnterTransition(
                isFirst,
                transitionDefinition,
                children
            )
        }
    }

    Stack(Modifier.fillMaxWidth() + Modifier.fillMaxHeight()) {
        Recompose {
            animState.items.forEach { item ->
                key(item.key) {
                    item.transition(it) {
                        AnimateTransition(state = it) {
                            children(item.key)
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun EnterTransition(
    firstChild: Boolean,
    transitionDefinition: TransitionDefinition<TransitionStates>,
    children: @Composable() (TransitionState) -> Unit
) {
    Transition(
        definition = transitionDefinition,
        initState = if (!firstChild) Enter else Active,
        toState = Active
    ) {
        children(it)
    }
}

@Composable
private fun ExitTransition(
    transitionDefinition: TransitionDefinition<TransitionStates>,
    children: @Composable() (TransitionState) -> Unit,
    onFinish: (TransitionStates) -> Unit = { }
) {
    Transition(
        definition = transitionDefinition,
        initState = Active,
        toState = Exit,
        onStateChangeFinished = onFinish
    ) {
        children(it)
    }
}

@Composable
private fun AnimateTransition(
    state: TransitionState,
    f: @Composable() () -> Unit
) {
    val x = state[X]
    val y = state[Y]
    val rotation = state[Rotation]
    val opacity = state[Opacity]
    val modifier = remember(x, y, rotation, opacity) {
        Modifier.drawWithContent {
            nativeCanvas.saveLayerAlpha(
                0f,
                0f,
                size.width.value,
                size.height.value,
                (opacity * 255).roundToInt()
            )
            val startX = size.width.value
            val startY = size.height.value
            translate(startX * x, startY * y)
            nativeCanvas.rotate(
                rotation,
                size.width.value / 2,
                size.height.value / 2
            )
            drawContent()
            restore()
        }
    }

    val clip = Modifier.clip(RectangleShape)

    Box(modifier = modifier + clip) {
        f()
    }
}

@Stable
private data class AnimationState<T>(
    var current: T? = null,
    val items: MutableList<AnimationItem<T>> = mutableListOf()
)

@Immutable
private data class AnimationItem<T>(
    val key: T,
    val transition: @Composable() (recompose: () -> Unit, children: @Composable() (TransitionState) -> Unit) -> Unit
)

object AnimationParams {
    val X = FloatPropKey()
    val Y = FloatPropKey()
    val Opacity = FloatPropKey()
    val Rotation = FloatPropKey()
}

enum class TransitionStates {
    Enter, Active, Exit
}

private val Defaults = listOf(
    X to 0f,
    Y to 0f,
    Opacity to 1f,
    Rotation to 0f
)

private fun TransitionDefinition<TransitionStates>.fillDefault(): TransitionDefinition<TransitionStates> {
    TransitionStates.values().forEach {
        val oldState = getStateFor(it) as MutableTransitionState
        // hack state
        Defaults.forEach { (key, value) ->
            try {
                oldState[key] = value
            } catch (e: IllegalArgumentException) {
                // value exists
            }
        }
    }
    return this
}

object Transitions {
    val Fade = transitionDefinition {
        state(Enter) {
            this[X] = 0f
            this[Y] = 0f
            this[Opacity] = 0f
            this[Rotation] = 0f
        }

        state(Active) {
            this[X] = 0f
            this[Y] = 0f
            this[Opacity] = 1f
            this[Rotation] = 0f
        }

        state(Exit) {
            this[X] = 0f
            this[Y] = 0f
            this[Opacity] = 0f
            this[Rotation] = 0f
        }

        transition(fromState = Enter, toState = Active) {
            Opacity using tween<Float> {
                duration = 300
                delay = 0
                easing = FastOutLinearInEasing
            }
        }
    }
}
