package com.dmi.util.gl

import android.content.Context
import android.opengl.GLES20.glDeleteProgram
import android.opengl.GLES20.glUseProgram

class GLProgram(vertexShader: String, fragmentShader: String) : GLResource {
    constructor(context: Context, vertexShaderResId: Int, fragmentShaderResId: Int) : this(
        context.resources.readShader(vertexShaderResId),
        context.resources.readShader(fragmentShaderResId)
    )

    val id = glCreateProgram(vertexShader, fragmentShader)
    override fun bind() = glUseProgram(id)
    override fun unbind() = glUseProgram(0)
    override fun dispose() = glDeleteProgram(id)
}