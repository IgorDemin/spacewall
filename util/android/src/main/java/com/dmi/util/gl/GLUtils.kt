package com.dmi.util.gl

import android.app.ActivityManager
import android.content.Context
import android.content.pm.ConfigurationInfo
import android.content.res.Resources
import android.graphics.Bitmap
import android.opengl.GLES20.*
import com.dmi.util.*
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.ShortBuffer
import java.util.*
import javax.microedition.khronos.egl.EGL10
import javax.microedition.khronos.egl.EGL11

const val BYTES_PER_INTEGER = 4
const val BYTES_PER_FLOAT = 4
const val BYTES_PER_SHORT = 2

fun floatBufferOf(items: FloatArray): FloatBuffer = ByteBuffer
    .allocateDirect(items.size * BYTES_PER_FLOAT)
    .order(ByteOrder.nativeOrder())
    .asFloatBuffer()
    .apply { put(items).position(0) }

fun floatBufferOf(size: Int): FloatBuffer = ByteBuffer
    .allocateDirect(size * BYTES_PER_FLOAT)
    .order(ByteOrder.nativeOrder())
    .asFloatBuffer()

fun shortBufferOf(items: ShortArray): ShortBuffer = ByteBuffer
    .allocateDirect(items.size * BYTES_PER_SHORT)
    .order(ByteOrder.nativeOrder())
    .asShortBuffer()
    .apply { put(items).position(0) }

fun compileShader(strSource: String, type: Int): Int {
    val ids = IntArray(1)
    val shader = glCreateShader(type)
    glShaderSource(shader, strSource)
    glCompileShader(shader)
    glGetShaderiv(shader, GL_COMPILE_STATUS, ids, 0)
    if (ids[0] == 0) {
        val typeStr = if (type == GL_VERTEX_SHADER) "vertex" else "fragment"
        val msg = glGetShaderInfoLog(shader)
        throw RuntimeException("Compile $typeStr shader failed: $msg")
    }
    return shader
}

fun glCreateProgram(resources: Resources, vertexShaderResId: Int, fragmentShaderResId: Int): Int {
    val vertexShader = resources.readShader(vertexShaderResId)
    val fragmentShader = resources.readShader(fragmentShaderResId)
    return glCreateProgram(vertexShader, fragmentShader)
}

fun Resources.readShader(resId: Int) = openRawResource(resId).use { it.reader().readText() }

fun glCreateProgram(vertexShader: String, fragmentShader: String): Int {
    val vShader = compileShader(vertexShader, GL_VERTEX_SHADER)
    val fShader = compileShader(fragmentShader, GL_FRAGMENT_SHADER)

    val progId = glCreateProgram()

    glAttachShader(progId, vShader)
    glAttachShader(progId, fShader)

    linkProgram(progId)

    glDeleteShader(vShader)
    glDeleteShader(fShader)

    return progId
}

private fun linkProgram(progId: Int) {
    val ids = IntArray(1)
    glLinkProgram(progId)
    glGetProgramiv(progId, GL_LINK_STATUS, ids, 0)
    if (ids[0] <= 0) {
        glGetProgramiv(progId, GL_INFO_LOG_LENGTH, ids, 0)
        val msg = glGetProgramInfoLog(progId)
        throw RuntimeException("Linking shader failed: $msg")
    }
}

fun glGenTexture(): Int {
    val ids = IntArray(1)
    glGenTextures(1, ids, 0)
    return ids[0]
}

fun glGenFrameBuffer(): Int {
    val ids = IntArray(1)
    glGenFramebuffers(1, ids, 0)
    return ids[0]
}

fun glGenBuffer(): Int {
    val ids = IntArray(1)
    glGenBuffers(1, ids, 0)
    return ids[0]
}

fun glDelTexture(id: Int) {
    val ids = IntArray(1) { id }
    glDeleteTextures(1, ids, 0)
}

fun glDelFrameBuffer(id: Int) {
    val ids = IntArray(1) { id }
    glDeleteFramebuffers(1, ids, 0)
}

fun glDelBuffer(id: Int) {
    val ids = IntArray(1) { id }
    glDeleteBuffers(1, ids, 0)
}

fun glCheckError() {
    val error = glGetError()
    if (error != 0) {
        throw RuntimeException("GL error: $error ${getEGLErrorString(error)}")
    }
}

fun getEGLErrorString(error: Int) = when (error) {
    EGL10.EGL_SUCCESS -> "EGL_SUCCESS"
    EGL10.EGL_NOT_INITIALIZED -> "EGL_NOT_INITIALIZED"
    EGL10.EGL_BAD_ACCESS -> "EGL_BAD_ACCESS"
    EGL10.EGL_BAD_ALLOC -> "EGL_BAD_ALLOC"
    EGL10.EGL_BAD_ATTRIBUTE -> "EGL_BAD_ATTRIBUTE"
    EGL10.EGL_BAD_CONFIG -> "EGL_BAD_CONFIG"
    EGL10.EGL_BAD_CONTEXT -> "EGL_BAD_CONTEXT"
    EGL10.EGL_BAD_CURRENT_SURFACE -> "EGL_BAD_CURRENT_SURFACE"
    EGL10.EGL_BAD_DISPLAY -> "EGL_BAD_DISPLAY"
    EGL10.EGL_BAD_MATCH -> "EGL_BAD_MATCH"
    EGL10.EGL_BAD_NATIVE_PIXMAP -> "EGL_BAD_NATIVE_PIXMAP"
    EGL10.EGL_BAD_NATIVE_WINDOW -> "EGL_BAD_NATIVE_WINDOW"
    EGL10.EGL_BAD_PARAMETER -> "EGL_BAD_PARAMETER"
    EGL10.EGL_BAD_SURFACE -> "EGL_BAD_SURFACE"
    EGL11.EGL_CONTEXT_LOST -> "EGL_CONTEXT_LOST"
    else -> "0x" + Integer.toHexString(error)
}

external fun texSubImage2D(
    target: Int, level: Int, xoffset: Int, yoffset: Int,
    bitmap: Bitmap, bitmapX: Int, bitmapY: Int, bitmapWidth: Int, bitmapHeight: Int
)

private val viewportSizes by threadLocal(ArrayDeque<Pos2>())

fun glWithViewport(size: Pos2, block: () -> Unit) {
    val old = viewportSizes.lastOrNull()
    viewportSizes.add(size)
    glViewport(0, 0, size.x, size.y)
    block()
    if (old != null) {
        glViewport(0, 0, old.x, old.y)
    }
    viewportSizes.removeLast()
}

fun glClip(region: Region2, block: () -> Unit) {
    glEnable(GL_SCISSOR_TEST)
    glScissor(region)
    block()
    glDisable(GL_SCISSOR_TEST)
}

fun glScissor(region: Region2) {
    val range = (region.coerceIn(Region2.ONE) * viewportSizes.last()).toRange()
    glScissor(range.start.x, range.start.y, range.size.x, range.size.y)
}

val Context.openGLVersion
    get(): Int {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val configInfo = activityManager.deviceConfigurationInfo
        return if (configInfo.reqGlEsVersion != ConfigurationInfo.GL_ES_VERSION_UNDEFINED) {
            configInfo.reqGlEsVersion
        } else {
            1 shl 16 // Lack of property means OpenGL ES version 1
        }
    }
