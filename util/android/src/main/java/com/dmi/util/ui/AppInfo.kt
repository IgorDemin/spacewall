package com.dmi.util.ui

import android.content.Context
import android.content.pm.PackageInfo

fun Context.appVersion(): String {
    val pInfo: PackageInfo = packageManager.getPackageInfo(packageName, 0)
    return pInfo.versionName
}
