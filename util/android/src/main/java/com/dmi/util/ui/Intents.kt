package com.dmi.util.ui

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.util.Log

fun Activity.openLink(
    link: String
) {
    try {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
        startActivity(browserIntent)
    } catch (e: Exception) {
        Log.e("spacewall", "openLink error", e) // TODO write into Analytics?
    }
}

fun Activity.openPlayMarket() {
    val uri = Uri.parse("market://details?id=$packageName")

    val intent = Intent(Intent.ACTION_VIEW, uri)
    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)

    val alternateIntent by lazy {
        Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=$packageName"))
    }

    try {
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            startActivity(alternateIntent)
        }
    } catch (e: Exception) {
        Log.e("spacewall", "openPlayMarket error", e) // TODO write into Analytics?
    }
}
