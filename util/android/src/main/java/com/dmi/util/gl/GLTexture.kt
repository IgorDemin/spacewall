package com.dmi.util.gl

import android.opengl.GLES20.*
import android.opengl.GLES30.GL_RED
import com.dmi.util.Pos2

@OptIn(ExperimentalStdlibApi::class)
class GLTexture(
    val size: Pos2,
    colorFormat: ColorFormat = ColorFormat.RGBA,
    wrap: Wrap = Wrap.CLAMP_TO_EDGE,
    filter: Filter = Filter.LINEAR
) : GLResource {
    val id = glGenTexture()

    init {
        bind {
            glTexImage2D(GL_TEXTURE_2D, 0, colorFormat.toGL(), size.x, size.y, 0, colorFormat.toGL(), GL_UNSIGNED_BYTE, null)
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap.toGL())
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap.toGL())
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter.toGL())
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter.toGL())
        }
    }

    private fun ColorFormat.toGL() = when (this) {
        ColorFormat.RGBA -> GL_RGBA
        ColorFormat.LUMINANCE -> GL_LUMINANCE
        ColorFormat.RED -> GL_RED
    }

    private fun Wrap.toGL() = when (this) {
        Wrap.CLAMP_TO_EDGE -> GL_CLAMP_TO_EDGE
        Wrap.REPEAT -> GL_REPEAT
    }

    private fun Filter.toGL() = when (this) {
        Filter.LINEAR -> GL_LINEAR
        Filter.NEAREST -> GL_NEAREST
    }

    override fun dispose() {
        glDelTexture(id)
    }

    override fun bind() {
        glBindTexture(GL_TEXTURE_2D, id)
        bound.add(this)
    }

    override fun unbind() {
        bound.removeLast()
        glBindTexture(GL_TEXTURE_2D, bound.lastOrNull()?.id ?: 0)
    }

    enum class ColorFormat { RGBA, LUMINANCE, RED }
    enum class Wrap { CLAMP_TO_EDGE, REPEAT }
    enum class Filter { LINEAR, NEAREST }

    companion object {
        private val bound = ArrayList<GLTexture>()
    }
}