package com.dmi.util.gl.obj

import com.dmi.util.Vec2
import com.dmi.util.div
import com.dmi.util.times

data class Projection2D(val offset: Vec2, val scale: Float = 1F)

operator fun Projection2D.div(scale: Float) = Projection2D(offset / scale, this.scale / scale)

operator fun Projection2D.times(scale: Float) = Projection2D(offset * scale, this.scale * scale)

operator fun Projection2D.plus(offset: Vec2) = Projection2D(this.offset * offset, scale)
