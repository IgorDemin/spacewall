package com.dmi.util.gl

import android.opengl.GLES20.*
import java.nio.FloatBuffer

class GLArrayBuffer(usage: Int, private val items: FloatBuffer) : GLResource {
    constructor(usage: Int, items: FloatArray) : this(usage, floatBufferOf(items))

    private val id = glGenBuffer()
    val size = items.remaining()

    init {
        bind {
            glBufferData(GL_ARRAY_BUFFER, size * BYTES_PER_FLOAT, items, usage)
        }
    }

    fun refresh() = bind {
        glBufferSubData(GL_ARRAY_BUFFER, 0, size * BYTES_PER_FLOAT, items)
    }

    override fun dispose() {
        glDelBuffer(id)
    }

    override fun bind() = glBindBuffer(GL_ARRAY_BUFFER, id)

    override fun unbind() = glBindBuffer(GL_ARRAY_BUFFER, 0)
}

class GLElementArrayBuffer(usage: Int, items: ShortArray) : GLResource {
    private val id = glGenBuffer()
    val size = items.size

    init {
        bind {
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, items.size * BYTES_PER_SHORT, shortBufferOf(items), usage)
        }
    }

    fun set(items: FloatArray) = bind {
        glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, items.size * BYTES_PER_FLOAT, floatBufferOf(items))
    }

    override fun dispose() {
        glDelBuffer(id)
    }

    override fun bind() = glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id)
    override fun unbind() = glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0)
}