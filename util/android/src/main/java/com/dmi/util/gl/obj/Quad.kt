package com.dmi.util.gl.obj

import android.content.Context
import android.opengl.GLES20.*
import com.dmi.util.Disposable
import com.dmi.util.R
import com.dmi.util.Region2
import com.dmi.util.gl.GLArrayBuffer
import com.dmi.util.gl.GLProgram
import com.dmi.util.gl.GLTexture
import com.dmi.util.gl.bind

class Quad(context: Context) : Disposable {
    private val positionsLength = 4

    private val program = GLProgram(context, R.raw.quad_vertex, R.raw.quad_fragment)

    private val handles = object {
        val position = glGetAttribLocation(program.id, "position")
        val texture = glGetAttribLocation(program.id, "texture")
        val textureOffset = glGetUniformLocation(program.id, "textureOffset")
        val textureSize = glGetUniformLocation(program.id, "textureSize")
    }

    private val positions = GLArrayBuffer(
        GL_STATIC_DRAW, floatArrayOf(
            -1F, -1F, 0F, 0F,
            1F, -1F, 1F, 0F,
            -1F, 1F, 0F, 1F,
            1F, 1F, 1F, 1F
        )
    )

    override fun dispose() {
        positions.dispose()
        program.dispose()
    }

    fun draw(texture: GLTexture, textureRegion: Region2 = Region2.ONE) = program.bind {
        glEnableVertexAttribArray(handles.position)
        positions.bind {
            glVertexAttribPointer(handles.position, positionsLength, GL_FLOAT, false, 0, 0)
        }

        glUniform2f(handles.textureOffset, textureRegion.start.x, textureRegion.start.y)
        glUniform2f(handles.textureSize, textureRegion.size.x, textureRegion.size.y)

        glUniform1i(handles.texture, 0)
        texture.bind {
            glDrawArrays(GL_TRIANGLE_STRIP, 0, positions.size / positionsLength)
        }
    }
}