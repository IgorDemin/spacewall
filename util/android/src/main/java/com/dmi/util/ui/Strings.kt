package com.dmi.util.ui

import android.text.Annotation
import android.text.SpannableString
import androidx.annotation.StringRes
import androidx.compose.Composable
import androidx.ui.core.ContextAmbient
import androidx.ui.material.MaterialTheme
import androidx.ui.text.AnnotatedString
import androidx.ui.text.SpanStyleItem
import androidx.ui.text.style.TextDecoration

@Composable
fun textLinkFrom(@StringRes id: Int): String {
    val context = ContextAmbient.current
    val text = context.resources.getText(id)
    val spannableString = SpannableString(text)
    val annotations = spannableString.getSpans(0, text.length, Annotation::class.java)
    return annotations.find { it.key == "link" }!!.value
}

@Composable
fun textResource(@StringRes id: Int): AnnotatedString {
    val context = ContextAmbient.current
    return context.resources.getText(id).replaceDefaultAnnotations()
}

@Composable
private fun CharSequence.replaceDefaultAnnotations(): AnnotatedString {
    val linkStyle = MaterialTheme.typography.body1
        .copy(textDecoration = TextDecoration.Underline, color = MaterialTheme.colors.primary)
        .toSpanStyle()

    val spannableString = SpannableString(this)
    val annotations = spannableString.getSpans(0, length, Annotation::class.java)

    val styles = ArrayList<SpanStyleItem>()

    for (annotation in annotations) {
        val start = spannableString.getSpanStart(annotation)
        val end = spannableString.getSpanEnd(annotation)

        when (annotation.key) {
            "link" -> {
                styles.add(SpanStyleItem(linkStyle, start, end))
            }
        }
    }

    return AnnotatedString(toString(), styles)
}
