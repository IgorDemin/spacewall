package com.dmi.util.ui

import com.dmi.util.Pos2
import org.amshove.kluent.shouldBeEqualTo
import org.junit.jupiter.api.Test

internal class BitmapsTest {
    @Test
    fun calculateSampleSize() {
        calculateSampleSize(Pos2(500, 500), Pos2(100, 100)) shouldBeEqualTo 5 // 500 / 5 = 100
        calculateSampleSize(Pos2(400, 500), Pos2(100, 100)) shouldBeEqualTo 4 // 400 / 4 = 100
        calculateSampleSize(Pos2(399, 500), Pos2(100, 100)) shouldBeEqualTo 3 // 399 / 3 = 133
        calculateSampleSize(Pos2(500, 1000), Pos2(100, 100)) shouldBeEqualTo 5 // 500 / 5 = 100
        calculateSampleSize(Pos2(500, 1000), Pos2(50, 100)) shouldBeEqualTo 10 // 500 / 10 = 50
    }
}
