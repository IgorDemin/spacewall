@file:Suppress("UnstableApiUsage")

plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
}

android {
    buildFeatures {
        compose = true
    }

    kotlinOptions {
        jvmTarget = "1.8"
        freeCompilerArgs = listOf(
            "-Xopt-in=kotlin.RequiresOptIn"
        )
    }

    composeOptions {
        kotlinCompilerVersion = composeKotlinCompilerVersion
        kotlinCompilerExtensionVersion = composeVersion
    }
}

dependencies {
    api("androidx.appcompat:appcompat:1.1.0")

    api("org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutinesVersion")
    api("androidx.compose:compose-runtime:$composeVersion")
    api("androidx.ui:ui-framework:$composeVersion")
    api("androidx.ui:ui-layout:$composeVersion")
    api("androidx.ui:ui-material:$composeVersion")
    api("androidx.ui:ui-foundation:$composeVersion")
    api("androidx.ui:ui-animation:$composeVersion")
    api("androidx.ui:ui-tooling:$composeVersion")
    kapt("io.arrow-kt:arrow-meta:$arrowVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.5.2")
    testImplementation("org.amshove.kluent:kluent:1.60")

    api(project(":util:common"))
}
