plugins {
    id("com.android.library")
    kotlin("android")
}

dependencies {
    api("android.arch.persistence.room:testing:$roomVersion")
    api("androidx.test:core:1.2.0")
    api("androidx.test:runner:1.2.0")
    api("androidx.test.ext:junit:1.1.1")

    api(project(":util:commonTest"))
}
