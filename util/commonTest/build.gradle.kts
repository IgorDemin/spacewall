plugins {
    kotlin("jvm")
    kotlin("kapt")
}

dependencies {
    api("org.junit.jupiter:junit-jupiter-engine:5.5.2")
    api("org.amshove.kluent:kluent:1.60")

    api(project(":util:common"))
}
