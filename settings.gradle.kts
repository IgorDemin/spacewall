include(
    ":util:common",
    ":util:commonTest",
    ":util:android",
    ":util:androidTest",
    ":db",
    ":wallpaper"
)