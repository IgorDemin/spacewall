import java.io.File
import java.util.*

fun loadProperties(path: String): Properties {
    val properties = Properties()
    val file = File(path)
    if (file.exists()) {
        properties.load(file.inputStream().buffered())
    }
    return properties
}
