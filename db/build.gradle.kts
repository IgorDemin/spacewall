plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
}

dependencies {
    api("androidx.room:room-runtime:$roomVersion")
    api("androidx.room:room-ktx:$roomVersion")
    implementation("com.github.MatrixDev.Roomigrant:RoomigrantLib:0.1.1")
    kapt("androidx.room:room-compiler:$roomVersion")
    kapt("com.github.MatrixDev.Roomigrant:RoomigrantCompiler:0.1.1")

    implementation(project(":util:android"))
    androidTestImplementation(project(":util:androidTest"))
}

android {
    defaultConfig {
        javaCompileOptions {
            annotationProcessorOptions {
                arguments = mapOf(
                    "room.schemaLocation" to "$projectDir/schemas",
                    "room.incremental" to "true"
                )
            }
        }
    }

    sourceSets["androidTest"].assets.srcDir("$projectDir/schemas")
}
