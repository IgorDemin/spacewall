package ru.wildberries.data.db

import androidx.room.testing.MigrationTestHelper
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MigrationTest {

    @get:Rule
    val helper = MigrationTestHelper(
        InstrumentationRegistry.getInstrumentation(),
        AppDatabase::class.java.canonicalName,
        FrameworkSQLiteOpenHelperFactory()
    )

    @Test
    fun testAutoMigrations() {
        val migrations = AppDatabase_Migrations.build()

        for (migration in migrations) {
            val helper = MigrationTestHelper(
                InstrumentationRegistry.getInstrumentation(),
                AppDatabase::class.java.canonicalName,
                FrameworkSQLiteOpenHelperFactory()
            )

            helper.createDatabase(DB_NAME, migration.startVersion).close()
            helper.runMigrationsAndValidate(DB_NAME, migration.endVersion, true, migration).close()
        }
    }

    companion object {
        private const val DB_NAME = "test"
    }
}
