package ru.wildberries.data.db.entity

import androidx.room.*

// TODO reverse ForeignKey.CASCADE for ConfigEntity/PresetEntity

@Entity(
    indices = [Index("configId", "imageId", unique = true)],
    foreignKeys = [
        ForeignKey(
            entity = ConfigEntity::class,
            onDelete = ForeignKey.CASCADE,
            parentColumns = ["id"],
            childColumns = ["configId"]
        ),
        ForeignKey(
            entity = ImageEntity::class,
            onDelete = ForeignKey.CASCADE,
            parentColumns = ["id"],
            childColumns = ["imageId"]
        )
    ]
)
data class PresetEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val configId: Long,
    val imageId: Long
)

@Dao
interface PresetDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun put(entity: PresetEntity): Long

    @Query("delete from PresetEntity where id in (:ids)")
    suspend fun delete(ids: List<Long>)

    @Query("SELECT * FROM PresetEntity")
    suspend fun all(): List<PresetEntity>
}
