package ru.wildberries.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import dev.matrix.roomigrant.GenerateRoomMigrations
import ru.wildberries.data.db.converter.FloatPairConverter
import ru.wildberries.data.db.converter.Vec3PairConverter
import ru.wildberries.data.db.entity.*

@Database(
    entities = [
        ConfigEntity::class,
        ImageEntity::class,
        PresetEntity::class,
        SettingsEntity::class
    ],
    version = 1
)
@TypeConverters(
    FloatPairConverter::class,
    Vec3PairConverter::class
)
@GenerateRoomMigrations
abstract class AppDatabase : RoomDatabase() {
    abstract fun configDao(): ConfigDao
    abstract fun imageDao(): ImageDao
    abstract fun presetDao(): PresetDao
    abstract fun settingsDao(): SettingsDao
}
