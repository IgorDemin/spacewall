package ru.wildberries.data.db.converter

import androidx.room.TypeConverter
import com.dmi.util.FloatPair

class FloatPairConverter {
    @TypeConverter
    fun toData(value: FloatPair): String = "${value.start};${value.end}"

    @TypeConverter
    fun fromData(value: String): FloatPair? {
        val values = value.split(";").map { it.toFloat() }
        return FloatPair(values[0], values[1])
    }
}
