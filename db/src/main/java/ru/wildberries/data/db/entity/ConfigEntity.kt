package ru.wildberries.data.db.entity

import androidx.room.*
import com.dmi.util.FloatPair
import com.dmi.util.Vec3

@Entity
class ConfigEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,

    val tile: Float,
    val velocitySpeed: Float,
    val velocityAngleDegree: Float,
    val stepsFloat: Float,

    val mainIsAntialiasing: Boolean,
    val mainResolution: Float,
    val starsResolution: FloatPair,

    val iterations: FloatPair,

    val param1: FloatPair,
    val param2: FloatPair,
    val param3: FloatPair,
    val param4: FloatPair,

    val backgroundHSV: Vec3,

    val gamma: FloatPair,
    val intensityPow: FloatPair,

    val hue: FloatPair,
    val saturation: FloatPair,
    val value: FloatPair,
    val alpha: FloatPair
)

@Dao
interface ConfigDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun put(entity: ConfigEntity): Long

    @Query("SELECT * FROM ConfigEntity WHERE id=:id LIMIT 1")
    suspend fun get(id: Long): ConfigEntity?
}
