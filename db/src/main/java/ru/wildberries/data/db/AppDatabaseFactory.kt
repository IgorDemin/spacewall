package ru.wildberries.data.db

import android.content.Context
import androidx.room.Room

fun AppDatabase(context: Context) = Room
    .databaseBuilder(context, AppDatabase::class.java, "app.db")
    .createFromAsset("database/app.db")
    .fallbackToDestructiveMigration()
    .addMigrations(*AppDatabase_Migrations.build())
    .build()
