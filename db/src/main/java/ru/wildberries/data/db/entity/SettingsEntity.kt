package ru.wildberries.data.db.entity

import androidx.room.*

@Entity(
    indices = [Index("configId", unique = true)],
    foreignKeys = [
        ForeignKey(
            entity = ConfigEntity::class,
            onDelete = ForeignKey.CASCADE,
            parentColumns = ["id"],
            childColumns = ["configId"]
        )
    ]
)
data class SettingsEntity(
    @PrimaryKey
    val id: Long = 1,
    val configId: Long = 0
)

@Dao
interface SettingsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun set(entity: SettingsEntity)

    @Query("SELECT * FROM SettingsEntity LIMIT 1")
    suspend fun get(): SettingsEntity?
}
