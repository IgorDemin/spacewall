package ru.wildberries.data.db.converter

import androidx.room.TypeConverter
import com.dmi.util.Vec3

class Vec3PairConverter {
    @TypeConverter
    fun toData(value: Vec3): String = "${value.x};${value.y};${value.z}"

    @TypeConverter
    fun fromData(value: String): Vec3? {
        val values = value.split(";").map { it.toFloat() }
        return Vec3(values[0], values[1], values[2])
    }
}
