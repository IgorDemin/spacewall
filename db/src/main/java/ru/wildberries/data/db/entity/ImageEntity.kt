package ru.wildberries.data.db.entity

import androidx.room.*

@Entity
class ImageEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    val imageJPG: ByteArray
)

@Dao
interface ImageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun put(entity: ImageEntity): Long

    @Query("SELECT * FROM ImageEntity WHERE id=:id LIMIT 1")
    suspend fun get(id: Long): ImageEntity?
}
